<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['cors']], function()
{
    Route::post('login', 'UserController@login');
    
    Route::resource('article-groups', 'ArticleGroupController');
    Route::group(['middleware' => ['auth:api']], function()
    {
        
        Route::post('updateImage/{id}/{model}', 'ArticleController@updateImage');
        Route::get('article-package/{articleId}', 'ArticleController@packageArticle');
        Route::get('get-package/{articleId}', 'PackageArticleController@getPackages');

        Route::post('filterBy', 'ArticleController@filterBy');
        Route::delete('deleteParticipant/{evId}/{participantId}', 'EventController@deleteParticipant');
        Route::get('article-mackeria/{id}', 'MackeriaArticleController@articleMackeria');
        Route::resource('articles', 'ArticleController');
        Route::resource('bookings', 'BookingController');
        Route::resource('customers', 'CustomerController');
        Route::resource('economies', 'EconomyController');
        Route::resource('economy-articles', 'EconomyArticleController');
        Route::resource('events', 'EventController');
        Route::resource('mackerias', 'MackeriaController');
        Route::resource('mackeria-articles', 'MackeriaArticleController');
        Route::resource('package-articles', 'PackageArticleController');
        Route::resource('participants', 'ParticipantController');
        Route::resource('partners', 'PartnerController');
		Route::get('settings/filterByName/{name}','SettingController@filterByName');
        Route::resource('settings', 'SettingController');
        Route::resource('users', 'UserController');

        /*

        // Template for API
        Route::get('test', 'TestController@index');
        Route::get('test/{id}', 'TestController@show');
        Route::post('test', 'TestController@store');
        Route::put('test/{id}', 'TestController@update');
        Route::delete('test/{id}', 'TestController@delete');
        Route::options('test', 'TestController@options'); // handles CORS
        Route::options('test/{id}', 'TestController@options'); // handles CORS

        */
    });
});