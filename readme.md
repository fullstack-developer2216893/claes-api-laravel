READ ME

## Composer
The Composer framework is used to define and install dependencies for the project.<br/>

## Migrations
Each table must have a migration file in order to recreate the MySQL table on various servers.<br/>
_See /database/migrations/2018_01_17_030636_create_tests_table.php_

## Seeds
Each table must have a seed file in order to quickly populate the table with test data.<br/>
_See /database/seeds/TestTableSeeder.php_

## Key Generation (API)
After running Composer install, please generate keys for the API.<br/>
_php artisan key:generate_

## Key Generation & Access Client (Passport)
After running Composer install, please generate keys and an Personal Access Client for Passport.<br/>
_php artisan passport:keys_</br>
_php artisan passport:client --personal_

## Reset database
Recreate database and populate with sample data.
_php artisan migrate:refresh --seed_