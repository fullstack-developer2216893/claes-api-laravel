-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2018 at 11:49 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `frodoapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `vat_level` int(11) NOT NULL DEFAULT '0',
  `article_group` int(11) NOT NULL DEFAULT '0',
  `result_unit` int(11) NOT NULL DEFAULT '0',
  `info` text COLLATE utf8mb4_unicode_ci,
  `layout1` int(11) NOT NULL DEFAULT '0',
  `layout2` int(11) NOT NULL DEFAULT '0',
  `layout3` int(11) NOT NULL DEFAULT '0',
  `bookable` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `date`, `name`, `unit`, `price`, `vat_level`, `article_group`, `result_unit`, `info`, `layout1`, `layout2`, `layout3`, `bookable`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1991-11-27', 'Ex consectetur accusantium angular', 1, 678, 1, 5, 2, 'Placeat quibusdam est quam officia cupiditate voluptatibus. Aliquam atque id et neque quis. Cum ipsa corrupti similique. Esse enim odit asperiores.', 23, 58, 16, 1, 'http://127.0.0.1/claes-api/storage/app/images/KCQKD2xZwae1YD43qPOBazYk3mUwv34vWQNBELNc.jpeg', NULL, '2018-03-01 13:44:58', '2018-03-27 16:58:31'),
(2, '2014-07-03', 'Rerum quae recusandae.', 2, 311, 0, 0, 0, 'Est voluptatem dolor est quo. Necessitatibus numquam alias delectus. Error alias quaerat vero. Est fuga provident ea quos accusantium exercitationem facilis.', 78, 76, 56, 1, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\de0967f5a92d359e251efe69ed4a79ba.jpg', NULL, '2018-03-01 13:44:59', '2018-03-01 13:44:59'),
(3, '2008-11-30', 'Aliquid sit fugit magni illum.', 3, 105, 0, 0, 0, 'Et quis repellat dolorem sapiente vitae. Expedita adipisci distinctio quibusdam assumenda rerum. Ullam eum blanditiis blanditiis odio aspernatur ut corporis nesciunt. Minima minima autem harum et et voluptatem. Vitae sed non fugiat inventore error consequatur qui et.', 75, 13, 45, 1, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\fb63322ecbd2cd6a0c03e4ff0a867734.jpg', NULL, '2018-03-01 13:45:00', '2018-03-01 13:45:00'),
(4, '2006-01-13', 'test edit 4', 1, 562, 0, 5, 0, 'test article', 14, 23, 76, 1, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\89dd0578ace2d95d8e5e1dd63add9d98.jpg', NULL, '2018-03-01 13:45:01', '2018-03-27 16:55:38'),
(5, '1972-09-16', 'Magni sit edit', 0, 860, 0, 3, 2, 'Provident quia error vitae inventore ipsa. Officia laboriosam repellat cupiditate inventore. In aut et nobis deserunt fugit illum.', 2, 28, 81, 0, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\26724a6957b5b551396a015d4d19e2eb.jpg', NULL, '2018-03-01 13:45:03', '2018-03-28 06:17:28'),
(6, '1978-04-21', 'Et ipsam molestiae quibusdam.', 0, 604, 0, 7, 2, 'Enim vel quam non quos praesentium reprehenderit. Voluptatem aut rerum optio expedita exercitationem est et. Nostrum rerum dolor quae repellat earum commodi. Blanditiis distinctio assumenda dolore nemo perspiciatis molestiae explicabo sit.', 50, 74, 94, 1, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\fc7287cf8d402d93bc8384ad95de49c1.jpg', NULL, '2018-03-01 13:45:04', '2018-03-27 11:47:20'),
(7, '2001-02-03', 'Ut et maiores.', 2, 343, 0, 10, 1, 'Veritatis est officia nihil praesentium id. Eius rerum sit earum in. Accusantium explicabo voluptatem quod accusantium in.', 34, 71, 57, 0, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\030cd65b1ab9c5dfeff00b223c250e37.jpg', NULL, '2018-03-01 13:45:06', '2018-03-01 13:45:06'),
(8, '2010-03-29', 'Autem laborum qui.', 3, 187, 0, 12, 2, 'Provident cumque qui ut et. Id saepe ullam dolore est necessitatibus in possimus. Id quo deserunt est neque nesciunt nam et dolorem. Enim sint neque nulla molestiae voluptas.', 76, 55, 85, 1, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\b9c2903b10035357dd64b8f4ebbac1c6.jpg', NULL, '2018-03-01 13:45:07', '2018-03-01 13:45:07'),
(9, '1976-09-01', 'Iste est cum.', 1, 403, 0, 3, 2, 'Quam labore odio dolor qui autem mollitia ex. Quaerat dicta placeat harum veritatis in quis. Quasi provident aut incidunt eos veritatis minus. Vitae at nobis commodi vel veniam maxime.', 2, 54, 66, 1, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\ab402b1e986e300b3e700a47d9d1039c.jpg', NULL, '2018-03-01 13:45:08', '2018-03-01 13:45:08'),
(10, '1977-12-7', 'Dolorum natus dolore.', 3, 184, 0, 9, 1, 'Vel ut suscipit autem. Dicta exercitationem expedita ut enim perspiciatis. Excepturi et dolore qui veritatis id.', 29, 13, 99, 1, 'C:\\Users\\jasma\\AppData\\Local\\Temp\\803bd834760b8f1f5098842301db45d3.jpg', NULL, '2018-03-01 13:45:09', '2018-04-12 04:58:30'),
(12, '2018-3-10', 'testing article', 1, 100, 1, 2, 1, 'Testing', 0, 0, 0, 1, 'http://127.0.0.1/claes-api/storage/app/images/mnnFuENd8VfZUXK65VZHXmSZ0DWGa7aFSFQJv265.jpeg', NULL, '2018-04-09 18:31:56', '2018-04-11 08:55:44'),
(14, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, NULL, NULL, '2018-04-11 12:26:58', '2018-04-11 12:26:58'),
(15, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, NULL, NULL, '2018-05-02 14:18:15', '2018-05-02 14:18:15'),
(16, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, NULL, NULL, '2018-05-02 14:19:52', '2018-05-02 14:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `article_groups`
--

CREATE TABLE `article_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article_groups`
--

INSERT INTO `article_groups` (`id`, `name`, `parent_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Arbetsplatser', -1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54'),
(2, 'Konferensrum', -1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54'),
(3, 'Konferenspaket', -1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54'),
(4, 'AV-utrustning', -1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54'),
(5, 'Café & Mackeria', -1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54'),
(6, 'Kontorsrum', 1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54'),
(7, 'Arbetsplatser', 1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54'),
(8, 'Flexiplatser', 1, NULL, '2018-03-01 13:44:54', '2018-03-01 13:44:54');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `economy_id` int(11) NOT NULL DEFAULT '0',
  `date_fr` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `article_id`, `economy_id`, `date_fr`, `date_to`, `created_at`, `updated_at`) VALUES
(1, 5, 6, '1980-09-24 04:48:07', '1994-11-02 01:16:20', '2018-03-01 08:15:09', '2018-03-01 08:15:09'),
(2, 8, 4, '2012-12-06 23:17:16', '1993-09-25 12:06:26', '2018-03-01 08:15:09', '2018-03-01 08:15:09'),
(3, 6, 5, '2008-07-01 18:00:06', '2016-03-20 10:37:08', '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(4, 4, 7, '2004-03-26 09:23:18', '1970-11-01 08:02:38', '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(5, 7, 1, '2007-01-23 06:54:49', '2005-11-24 20:21:38', '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(6, 6, 9, '2005-04-19 17:05:08', '1996-11-08 21:38:03', '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(7, 9, 2, '2008-08-27 18:46:35', '1980-04-12 09:04:05', '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(8, 5, 5, '1973-03-22 01:16:00', '2014-10-24 13:31:55', '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(9, 9, 7, '1988-11-21 17:09:23', '1973-08-02 18:59:49', '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(10, 7, 5, '2008-02-15 13:23:47', '1986-01-23 00:02:20', '2018-03-01 08:15:10', '2018-03-01 08:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workplace` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_category` int(11) NOT NULL DEFAULT '-1',
  `member_category` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visiting_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visiting_zip_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visiting_town` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `date`, `workplace`, `room_category`, `member_category`, `name`, `registration_number`, `phone`, `email`, `address`, `zip_code`, `town`, `visiting_address`, `visiting_zip_code`, `visiting_town`, `info`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2014-03-23', 'Voluptatem', 2, 1, 'Bartoletti-Welch', '817475217', '+1370381200173', 'info@itaque.com', '899 Ryley Stravenue', '37955', 'Dorachester', '259 Emmie Keys', '93507', 'Assuntaland', 'Voluptatem eos ut magni optio sed laboriosam est. Animi eum nostrum nesciunt odit optio nostrum ducimus. Ipsa numquam veniam et debitis. Aspernatur delectus perspiciatis repudiandae qui possimus assumenda similique pariatur.', NULL, '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(2, '2004-05-31', 'In', 2, 1, 'Rempel-Strosin', '678087516', '+8860452960538', 'info@rerum.com', '83728 Manley Parkway', '29279', 'Armstrongchester', '26011 Lydia Mall', '23761', 'Anikafort', 'Explicabo in quisquam tenetur eos doloremque sit molestiae. Iste vero enim consectetur doloribus voluptatum nihil dicta voluptatem. Eveniet omnis laborum sit amet omnis. Qui laboriosam qui magni minus temporibus inventore. Eos voluptate consequuntur et eveniet reiciendis numquam.', NULL, '2018-03-01 08:15:10', '2018-03-01 08:15:10'),
(3, '1993-05-04', 'Nihil', 1, 0, 'Murray-Eichmann', '588273348', '+2540089100979', 'info@iusto.com', '71209 Gislason Tunnel Suite 082', '93138', 'Doylestad', '46654 Earnest Estate', '52529', 'Marlenberg', 'Expedita harum sint dolore enim nostrum laudantium quos. Doloribus et atque saepe vitae quia cupiditate vel quibusdam. Laudantium rem qui ad sapiente dolores et non.', NULL, '2018-03-01 08:15:10', '2018-03-01 08:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `economies`
--

CREATE TABLE `economies` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_quote` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_version_quote` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_booking` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_order` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_conditions` text COLLATE utf8mb4_unicode_ci,
  `valid_to` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `delivery_terms` text COLLATE utf8mb4_unicode_ci,
  `room_info` text COLLATE utf8mb4_unicode_ci,
  `av_info` text COLLATE utf8mb4_unicode_ci,
  `price_info` text COLLATE utf8mb4_unicode_ci,
  `meal_info` text COLLATE utf8mb4_unicode_ci,
  `number_of_persons` int(11) NOT NULL DEFAULT '0',
  `number_of_persons_text` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `total` int(11) NOT NULL DEFAULT '0',
  `vat_total` int(11) NOT NULL DEFAULT '0',
  `total_incl_vat` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `economies`
--

INSERT INTO `economies` (`id`, `date`, `serial_quote`, `serial_version_quote`, `serial_booking`, `serial_order`, `customer_id`, `name`, `payment_conditions`, `valid_to`, `category`, `type`, `status`, `delivery_terms`, `room_info`, `av_info`, `price_info`, `meal_info`, `number_of_persons`, `number_of_persons_text`, `info`, `total`, `vat_total`, `total_incl_vat`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2001-08-10', '79754', '32930', '34077', '76108', 1, 'Quo Ratione Eos Mollitia Sint Aliquid Et Doloremque.', 'Quia et maiores ea vel perferendis cum. Dolorem qui qui illo quisquam. Facilis sunt omnis ab debitis ex aliquid sed. Minus voluptas rerum est. Corporis enim voluptatem alias nostrum velit sequi quia.', '2013-02-20', 0, 0, 1, 'Quas nesciunt vel sunt corporis laborum quis sint exercitationem. Repellendus a voluptatem et aperiam voluptatem est. Quo rem velit rerum provident est odio. Et tempore quia voluptas quidem molestias.', 'Doloremque ea eum tempore architecto nihil quae. Sed corrupti ea sequi amet dolores ut atque. Amet et omnis dolore quam consequatur quae quo.', 'Id a aliquam consequatur omnis sed. Beatae laboriosam aperiam est rerum similique voluptate. Facere iusto pariatur ducimus earum quod.', 'Rerum nulla cum vitae expedita aut. Qui et quod velit ducimus earum nesciunt fugiat. Sed molestiae quam distinctio illum. Ut illo voluptatibus autem rerum asperiores quia nisi dolor.', 'Voluptatum officiis quos non veritatis quisquam placeat voluptatum. Et laboriosam modi ut temporibus et. Nesciunt vero explicabo sapiente dolor omnis sit.', 58, 'Est Iure Voluptas Maiores Quaerat Non.', 'Enim ea voluptas velit occaecati. Qui rerum tenetur consequatur. Et maiores aspernatur nisi sapiente incidunt aut. Ducimus nam quia accusantium vel.', 6369, 1592, 7961, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(2, '2012-01-26', '26361', '67990', '17020', '84478', 1, 'Suscipit Suscipit Rerum Repudiandae Quis Qui.', 'Voluptatem perspiciatis voluptatem modi officiis qui. Aliquid voluptate modi fugit vero. Error enim in nulla ea in ut. Et dolores dolore est officiis.', '1975-12-03', 6, 3, 7, 'Minima dolorem rerum non quaerat beatae. Sequi accusamus qui reiciendis nisi molestiae. Rerum aspernatur officiis eos sit alias et quia enim.', 'Voluptatem ut facilis est error incidunt placeat aut harum. Temporibus temporibus sequi at excepturi deserunt. Sequi quos est in cumque enim. Nam aspernatur cumque at est odit eius est qui.', 'Animi eveniet nihil aut rerum sit. Ipsum recusandae mollitia id quibusdam in accusamus. Aut aperiam est deleniti animi.', 'Consectetur esse ut occaecati. Vero a iusto eum laudantium iure enim. Neque dolor reiciendis non est. Qui officiis a quidem quisquam nihil aut aut.', 'Dolorum aperiam fuga debitis asperiores ipsum exercitationem. Voluptatem vel dolor rerum pariatur quia. Vitae vel non in. Sint mollitia numquam dicta.', 81, 'Impedit Esse Quia Error Odit Dolorum Nulla Beatae.', 'Quia quia est enim nemo maiores iure. Quia placeat ipsam voluptatum eum rerum ipsum. Inventore possimus soluta et consectetur ratione sed.', 7168, 1792, 8960, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(3, '1982-03-21', '81132', '55294', '4574', '86991', 1, 'Earum Nisi Officiis Similique.', 'Amet sit deleniti ad deleniti. Maxime et amet qui alias dolore rerum et beatae. Voluptatem adipisci ipsam unde qui consequatur.', '1979-06-28', 0, 8, 3, 'Qui est atque quia consequuntur enim earum. Incidunt saepe explicabo laudantium ea voluptates. Ut commodi velit accusantium placeat doloremque aut nemo.', 'Autem alias dicta voluptas dolores ducimus occaecati. Qui neque amet nostrum nihil quidem ut. Eum libero eius et beatae dignissimos at animi.', 'Molestiae voluptas maxime ut est. Voluptas sit reiciendis repudiandae accusantium neque aspernatur quo. Eveniet qui voluptatibus in excepturi. Dolore omnis eum veniam et eum. Eius doloribus dolores repellat sint ipsum culpa consequatur facere.', 'Et illo consequatur voluptate voluptates dolores et. Asperiores reprehenderit consectetur nobis odit maxime suscipit praesentium. Earum nobis a maxime officia.', 'Dolorem id et dolor asperiores doloribus beatae velit. Assumenda vel atque aut et rerum. Ut officiis nostrum perspiciatis. Accusantium optio eos nisi totam cupiditate.', 14, 'Voluptates Aspernatur Ipsum Ad Consequuntur.', 'Dolor ut et qui sunt. Vel voluptatum architecto repudiandae rerum hic illum unde. Quia unde hic facilis corporis.', 6556, 1639, 8195, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(4, '2001-11-01', '38855', '2985', '69863', '24311', 2, 'Delectus Dicta Ut Quisquam Animi.', 'Consequatur earum quia aperiam. Voluptatem doloribus rerum distinctio quas consectetur provident. Dolore et recusandae nihil et et nobis sed. Non deserunt aut ut accusantium modi et necessitatibus.', '1976-01-09', 1, 1, 9, 'Quo nobis vel explicabo vitae distinctio sit odio. Debitis eius sit sit id. Perferendis et at tempora quam mollitia. Esse aut qui est ut.', 'Ut dignissimos sapiente consequatur exercitationem beatae ex quis. Temporibus aperiam aut perferendis corrupti laborum. Sit commodi debitis dolores rerum et sint assumenda vitae. Qui voluptatem aliquid nesciunt et facilis.', 'Saepe occaecati doloremque blanditiis in et sit omnis. Quia est placeat natus doloribus laudantium veniam. Enim quam nostrum consequuntur minus.', 'Ipsam adipisci cumque amet dolorum ipsa vero cupiditate. Ipsum impedit omnis nihil consequatur saepe adipisci delectus. Consectetur debitis eos tempore et. Aperiam qui dolor ipsam ab.', 'Magni cumque optio qui. Ab asperiores accusamus dolor. Quaerat ex dolorem qui et aperiam.', 90, 'Quod Aliquam Quia Nulla Incidunt.', 'Aut eum molestiae laudantium in. Enim eum consectetur et voluptas. Voluptatem sint minima possimus inventore et corrupti.', 9703, 2426, 12129, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(5, '1976-06-22', '23880', '33880', '33019', '23256', 2, 'Harum Dolorem Ullam Et Omnis Maiores Neque Vitae Itaque.', 'Et eum magni autem quibusdam harum. Consequuntur dolor nisi non nulla nulla quasi. Ut dolor numquam aut possimus fugit. Omnis omnis aliquam molestias excepturi et ea aut.', '1971-01-06', 1, 9, 3, 'Sunt maxime eius earum eum. Nisi quidem dignissimos vero ut perferendis eaque dolorem. Enim tempora voluptatem perspiciatis quo.', 'Est illo ipsum non quia et eos. Fugiat pariatur consequuntur libero totam quis sint. Rerum consequatur eligendi excepturi qui et. Illo totam consequuntur quo ut.', 'Labore et nesciunt qui itaque. Voluptas autem omnis harum non sequi. Corporis autem dolor fugiat quas impedit voluptate quaerat.', 'Culpa a reprehenderit adipisci ut veniam maiores ea inventore. Saepe nesciunt possimus sint nisi repudiandae velit facilis. Omnis et placeat ipsa cumque eveniet. Sunt ipsum totam voluptatem quasi.', 'Sint et placeat totam ut. Cupiditate cupiditate error ipsa sit ex. Itaque dolor officia aperiam numquam. Et tempore et sit enim rem adipisci et. Vero deleniti qui quia quia nobis dolore aut.', 66, 'Rerum Sapiente Et Natus Repellat Dolore.', 'Qui minima est quo ut. Sed magni optio ex placeat reiciendis aliquam in. Sit soluta nostrum quasi sunt commodi. Perspiciatis consequatur modi minima aut.', 4963, 1241, 6204, NULL, '2018-03-01 08:15:13', '2018-03-01 08:15:13'),
(6, '1982-01-12', '48822', '27618', '27426', '17441', 2, 'Modi Facere Laudantium Laboriosam Molestias Reiciendis.', 'Vel dolores ipsum esse culpa sit harum placeat. Sed voluptatum ducimus nemo minima. Omnis unde et eos cumque.', '1980-11-24', 8, 9, 4, 'Et ipsam facilis corporis. Repudiandae enim quod molestias dolore.', 'Voluptas quia magnam natus velit exercitationem. In sint exercitationem excepturi illo et commodi. Itaque quas perferendis sit at aliquam. Ratione vel est quod doloribus ut illum. Eligendi quidem omnis omnis maxime.', 'Nulla fuga occaecati ducimus voluptas ipsam recusandae modi. Ea eligendi et quae iure qui et debitis. Est beatae voluptates necessitatibus libero.', 'Et in ex cum ducimus hic non velit. Quae ut ad autem dolore mollitia ut provident. Nesciunt qui magni culpa quia.', 'Vel a voluptate et consequatur rerum accusamus. Laborum natus quasi pariatur. Explicabo ut deserunt iste cupiditate et excepturi. Minima nisi dicta molestiae aut dicta sed beatae.', 4, 'Alias Ipsam Sit Quas Unde.', 'Corrupti maxime ea odio facilis inventore autem. Consequuntur laborum ut ut est cum reprehenderit et. Quo sunt ullam praesentium perferendis voluptatum. Officiis reiciendis voluptas saepe iusto.', 5553, 1388, 6941, NULL, '2018-03-01 08:15:13', '2018-03-01 08:15:13'),
(7, '2009-11-05', '99264', '20780', '82572', '11599', 3, 'Cupiditate Magni Beatae Nihil Sit Ea Dolores Explicabo Incidunt.', 'Minima voluptates sed dolorem qui pariatur dolorum. Quis consequatur aut ratione aut omnis sit dolor.', '1990-05-03', 0, 0, 8, 'Vel explicabo corporis sed doloribus necessitatibus accusantium. Accusamus ratione tempore sint.', 'Cumque reprehenderit voluptate velit est. Accusantium ut est placeat in. Necessitatibus quidem reiciendis voluptates. Cupiditate in rerum illo culpa eum.', 'Sunt aut ut corporis quo consequatur molestias. Id officiis natus quia porro qui. In iure soluta eius amet debitis.', 'Tempora fuga dicta quo est sapiente ut. Velit rerum et quos earum facilis aut voluptatem. Ea ad nam consequatur facilis.', 'Qui architecto libero omnis tenetur. Placeat mollitia repellat quibusdam. Ut sapiente alias deleniti. Doloremque dolores nisi quos debitis.', 1, 'Consequatur Consequatur Aut Ut Similique Nihil Quibusdam.', 'Soluta delectus voluptas ipsum ea est qui. Dolorum id consequatur et suscipit quae animi tempore. Dolores ut quis debitis nisi ut quidem est. Voluptatem qui consequatur voluptatibus tenetur eveniet dolores fugiat.', 5286, 1322, 6608, NULL, '2018-03-01 08:15:13', '2018-03-01 08:15:13'),
(8, '1989-05-08', '35152', '99011', '15394', '84742', 3, 'Adipisci Minima Voluptate Delectus Dignissimos Ut.', 'Ut quia soluta sit et vero maxime et. Fugit quis ex totam quia repudiandae voluptate dolor pariatur. Est ut ea quidem ipsum. Commodi aspernatur officia a molestiae alias.', '2006-06-15', 5, 6, 0, 'Eos laborum sunt impedit repellendus. Eaque perferendis distinctio aut ut nihil. Eveniet ratione ab minima enim. Tenetur distinctio atque esse praesentium voluptatem magni consequatur.', 'Ut dolor ea quia aspernatur alias laudantium. Nesciunt laudantium nam molestiae incidunt sit beatae.', 'Dolor ea harum sunt. Alias aliquid sit delectus libero repudiandae. Sequi optio minima alias dolores. Velit qui et beatae officiis.', 'Omnis quam qui minus. Mollitia asperiores non est voluptatem. Temporibus libero omnis veniam ut quisquam laboriosam voluptatem.', 'Et in velit perspiciatis voluptatem. Aut alias aliquid eveniet voluptate. Velit ut qui aut occaecati.', 51, 'Similique Dicta Ut Perferendis Voluptatem Rerum Rerum.', 'Eos necessitatibus veritatis pariatur sit dolores. Laboriosam qui optio incidunt laudantium consequatur est. Odit doloremque ex dolorem est blanditiis.', 2208, 552, 2760, NULL, '2018-03-01 08:15:13', '2018-03-01 08:15:13'),
(9, '2013-04-19', '49192', '46709', '38764', '44902', 3, 'Commodi Et Sit Magni Minus Ratione.', 'Recusandae soluta labore odio voluptatem. Nulla vitae aut earum porro aut id dolorem deserunt. Dolore aut quis qui quaerat doloribus.', '2008-07-05', 8, 3, 2, 'Quo tempore sapiente velit at sit ut provident. Vero est voluptatem voluptatibus dolores occaecati. Sunt tempore maxime sit culpa. Quos qui natus ut tempore asperiores laboriosam.', 'Dolorem laudantium reiciendis qui et quidem a aut. Dolorem est quis ea omnis natus. Eos sint ut praesentium nobis doloribus quam impedit. Autem temporibus tenetur voluptatum quas qui.', 'Nihil numquam rerum commodi. Maiores omnis nesciunt est aut enim numquam totam suscipit. Animi similique libero excepturi aperiam non.', 'Quo tempore et nostrum voluptatem cupiditate est iste. Nam quisquam incidunt veritatis maxime consequatur. Ad ut ut fugit dolores earum.', 'Sint labore vel magni et. Ut debitis quia aperiam qui illum. Deleniti aperiam sit eius dignissimos tempore qui vel. Quia eius possimus assumenda.', 75, 'Et Quos Et Suscipit Dolores.', 'Hic quo aspernatur non corrupti minus. Accusamus cum aliquam voluptates possimus id voluptas. Facilis cum similique itaque enim ut dignissimos officiis.', 1917, 479, 2396, NULL, '2018-03-01 08:15:13', '2018-03-01 08:15:13'),
(10, '1997-08-07', '40319', '35348', '58055', '89618', 3, 'Ullam Sit Nisi Quidem.', 'Ut rerum voluptas numquam adipisci dolorem voluptatum. Quas incidunt sit asperiores quo ratione iusto. Magni possimus non consectetur sapiente facilis iure.', '2012-07-07', 7, 4, 0, 'Sed sint vel modi molestiae. Nihil esse consequatur rerum qui repudiandae. Ut debitis doloremque voluptates praesentium ut voluptatem.', 'Nostrum voluptas nemo nostrum earum. Voluptatem est rem modi non voluptates impedit quia inventore. Voluptates quia voluptates perspiciatis et. Enim iure tempore in beatae ut. Nostrum pariatur quis hic numquam tempore aspernatur deserunt.', 'Vitae quod consectetur dolores impedit provident perferendis. Culpa nisi aut quo distinctio blanditiis omnis. Rerum quisquam et repudiandae nam.', 'Qui et et ut qui libero. Libero at dolore consequuntur in facilis. Praesentium qui tempora corporis. Ut voluptatem iusto sit nihil dolorum suscipit autem.', 'Et fuga sapiente laborum exercitationem autem. Nulla sint exercitationem quia enim officiis similique dolorum repellat. Incidunt sit aut repudiandae vitae sunt aut.', 68, 'Ut Tempora Inventore Molestias Ex Voluptas.', 'Omnis ex et tempore. Optio quibusdam molestiae quos perferendis. Laboriosam omnis ipsam et quo quia repellendus porro. Beatae quod expedita occaecati tenetur.', 4853, 1213, 6066, NULL, '2018-03-01 08:15:13', '2018-03-01 08:15:13');

-- --------------------------------------------------------

--
-- Table structure for table `economy_articles`
--

CREATE TABLE `economy_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `quote_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL DEFAULT '0',
  `article_group` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `article_id` int(11) NOT NULL DEFAULT '0',
  `date_fr` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `serv_time` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `unit` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `vat_level` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  `vat_total` int(11) NOT NULL DEFAULT '0',
  `total_incl_vat` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `economy_articles`
--

INSERT INTO `economy_articles` (`id`, `order_id`, `quote_id`, `booking_id`, `article_group`, `name`, `package`, `quantity`, `article_id`, `date_fr`, `date_to`, `serv_time`, `discount`, `unit`, `price`, `vat_level`, `total`, `vat_total`, `total_incl_vat`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 9, 9, 8, 2, 'Tenetur Natus Animi Harum Consequatur Et Consequatur Fuga Illo.', 'Tempore Odio Quia Quis In Esse Qui Tempora.', 6, 5, '1994-11-04 11:54:00', '1976-11-14 08:03:23', '19:22', 22, 0, 7201, 25, 33701, 8425, 42126, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(2, 0, 5, 5, 1, 'Autem Rerum Incidunt Aspernatur Accusamus.', 'At Voluptatibus Quia Rem Nostrum.', 5, 3, '1975-04-02 17:47:39', '1994-12-18 07:08:19', '01:17', 69, 6, 6154, 25, 9539, 2385, 11923, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(3, 1, 2, 4, 6, 'Qui Cupiditate Nobis Dolorem.', 'Voluptates Facere Placeat Ut Enim Voluptatem.', 1, 2, '2011-02-12 08:13:57', '2009-02-14 04:52:43', '06:20', 79, 6, 7350, 25, 1543, 386, 1929, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(4, 3, 1, 2, 4, 'Quia Id Ipsum Accusantium.', 'Vitae Sit Voluptatem Ex Qui.', 4, 9, '1995-08-18 09:34:40', '2003-01-22 06:05:15', '23:35', 29, 9, 4314, 25, 12252, 3063, 15315, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(5, 7, 0, 1, 0, 'Tempora Enim Officia Ad Consequatur Sint Qui.', 'Quia Distinctio Numquam Aliquid Placeat Qui Aut.', 6, 7, '1993-10-11 04:16:17', '1978-11-08 07:00:32', '15:08', 83, 0, 1425, 25, 1454, 363, 1817, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(6, 3, 3, 3, 3, 'Molestiae Voluptas Voluptates Ullam Eius.', 'Accusamus Atque Totam Quae Ut Molestias Voluptatem Quos Rerum.', 0, 1, '2013-07-13 02:22:27', '1978-12-19 02:14:26', '15:50', 58, 5, 4974, 25, 0, 0, 0, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(7, 1, 0, 1, 8, 'Est Beatae Et Pariatur.', 'Eius Labore Quis Odit Sed Doloribus Quam Repellendus Tempora.', 2, 3, '1994-12-25 11:33:18', '1982-06-28 02:58:58', '18:19', 51, 2, 4114, 25, 4032, 1008, 5040, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(8, 1, 7, 1, 8, 'Odit Impedit Quia Et Delectus Quas.', 'Optio Quam Fuga Velit Placeat Nostrum Sunt Unde Aut.', 4, 3, '2002-01-07 22:10:41', '1986-02-26 06:10:08', '01:21', 22, 8, 8343, 25, 26030, 6508, 32538, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(9, 3, 7, 6, 8, 'Occaecati Numquam Neque Et Id Aliquid Consequatur.', 'Voluptas Et Ut Voluptatem.', 5, 3, '1981-07-15 13:52:05', '1984-01-10 04:17:21', '05:30', 55, 1, 4348, 25, 9783, 2446, 12229, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(10, 5, 1, 0, 1, 'Omnis Nulla Aut Suscipit Magni Quia Ad.', 'Ut Consequuntur Sint Delectus Distinctio.', 2, 1, '1992-09-24 17:59:47', '1978-03-12 02:48:28', '18:46', 75, 9, 2283, 25, 1142, 285, 1427, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(11, 7, 0, 8, 7, 'Rerum Dolorem Qui Vel Voluptatum Repellat.', 'Animi Tempore Eum Reiciendis Consequatur Consectetur.', 0, 8, '1991-08-14 19:35:23', '2017-03-08 10:24:03', '06:45', 95, 5, 5881, 25, 0, 0, 0, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(12, 6, 5, 5, 3, 'Et Nemo Maxime Qui Sed Beatae Quas Voluptatibus.', 'Et Mollitia Quae Nobis Est Rerum Sint Amet Nam.', 0, 2, '2003-04-16 08:21:05', '1984-03-03 09:16:26', '08:15', 88, 6, 176, 25, 0, 0, 0, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(13, 8, 1, 8, 9, 'Magni Qui Non Id Hic Quas Consequatur.', 'Accusamus Omnis Enim Est Labore In Sint Eum Ut.', 5, 3, '1985-04-17 02:56:58', '1975-02-03 15:02:58', '00:49', 22, 3, 4743, 25, 18498, 4624, 23122, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(14, 0, 6, 0, 4, 'Dolores Aspernatur Quos Dignissimos.', 'Asperiores Ullam Nemo Qui Asperiores.', 4, 0, '2000-08-05 15:44:53', '1970-12-07 08:49:10', '19:22', 4, 0, 1615, 25, 6202, 1550, 7752, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(15, 2, 2, 5, 8, 'Magnam Expedita Exercitationem Quisquam Illo Est.', 'Quaerat Nesciunt Quia Nulla Unde Pariatur.', 4, 8, '1995-05-05 02:22:45', '1986-02-23 16:20:04', '15:45', 12, 8, 4147, 25, 14597, 3649, 18247, NULL, '2018-03-01 08:15:11', '2018-03-01 08:15:11'),
(16, 4, 1, 0, 2, 'Natus Est Eveniet Architecto Et.', 'Eos Sapiente Quidem Quis Rem.', 3, 1, '2003-06-29 21:11:09', '2016-11-24 20:38:52', '06:28', 99, 1, 2452, 25, 74, 18, 92, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(17, 4, 7, 8, 0, 'Molestiae Aut Aliquid Voluptates Et Sed Culpa.', 'Qui Dolor Voluptatem Provident Ullam.', 5, 0, '1974-09-09 07:29:51', '2005-04-28 03:44:58', '21:10', 97, 2, 2620, 25, 393, 98, 491, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(18, 9, 9, 7, 5, 'Nostrum Laudantium Facere Facere Sed Eveniet Sint Dolor.', 'Ullam At Et Aliquam Sunt Beatae Non Natus.', 6, 6, '2014-09-04 11:51:10', '1995-03-03 13:40:45', '05:38', 6, 7, 6128, 25, 34562, 8640, 43202, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(19, 2, 3, 9, 4, 'At Magni Et Amet Nobis Est Numquam Iusto.', 'Ut Eius Est Ab Perferendis Error.', 8, 2, '1989-11-28 12:13:25', '1977-11-17 13:23:08', '12:48', 91, 7, 2839, 25, 2044, 511, 2555, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(20, 2, 2, 3, 7, 'At Eligendi In Natus Inventore Et Unde Sit.', 'Iure Sed Voluptas Minima.', 0, 8, '1973-10-28 23:04:43', '2012-10-28 22:30:00', '21:02', 90, 6, 1288, 25, 0, 0, 0, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(21, 8, 2, 7, 4, 'Dolorem Qui Occaecati Modi.', 'Vitae Iure Rerum Libero Unde Sunt Ipsam Voluptatum Sit.', 0, 7, '1990-12-04 13:41:42', '2017-06-13 06:10:06', '13:05', 38, 4, 5077, 25, 0, 0, 0, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(22, 5, 4, 7, 9, 'Consequatur Labore Enim Voluptate Rerum Cum Libero Aut.', 'Pariatur Ea Debitis Dignissimos Aut Natus Culpa Eum.', 5, 8, '1996-11-13 11:32:50', '1985-06-17 18:02:54', '17:36', 65, 8, 7518, 25, 13156, 3289, 16446, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(23, 1, 2, 2, 9, 'Commodi Itaque Quisquam Similique Sed Magni Est Natus Id.', 'Quaerat Repellendus Dignissimos Enim.', 7, 3, '1991-12-26 03:35:21', '1982-11-12 23:52:03', '21:57', 41, 9, 8147, 25, 33647, 8412, 42059, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(24, 1, 5, 3, 3, 'Quaerat Sunt Dolor Nemo Nulla.', 'Velit Incidunt Accusantium Vel Possimus Incidunt Et Aut.', 7, 0, '1994-06-08 10:47:05', '1997-08-24 05:15:42', '23:57', 37, 4, 4952, 25, 21838, 5460, 27298, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(25, 6, 8, 2, 4, 'Necessitatibus Dicta Animi Ullam Velit.', 'Eaque Dolorum Sint Beatae Officia Eos.', 6, 0, '1987-04-30 00:25:24', '2009-07-17 10:56:20', '08:39', 0, 5, 7249, 25, 43494, 10874, 54368, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(26, 9, 8, 1, 4, 'Sed Aut Qui Molestias Dolorum Tempora Qui Repudiandae.', 'Aliquid Adipisci Cumque Vitae Saepe Minus Qui.', 9, 5, '1984-06-25 23:23:56', '1986-05-19 17:22:38', '06:29', 13, 2, 3387, 25, 26520, 6630, 33150, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(27, 5, 2, 2, 8, 'Quibusdam Quis Harum Expedita Quia Iusto Aut Quisquam.', 'Quod Velit Rerum Et Quibusdam.', 3, 7, '2016-03-05 03:20:55', '1992-04-19 08:16:26', '14:19', 11, 8, 5077, 25, 13556, 3389, 16944, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(28, 5, 2, 3, 2, 'Perspiciatis Maxime Numquam Dolor Aperiam Corrupti Libero Nulla Ut.', 'Autem Odit Excepturi Esse.', 2, 8, '1972-09-15 15:35:43', '2010-09-28 06:12:31', '15:03', 69, 9, 2496, 25, 1548, 387, 1934, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(29, 4, 8, 3, 4, 'Ipsa Aliquam Cupiditate Aut Qui.', 'Impedit Repudiandae Asperiores Accusamus.', 8, 5, '1973-06-05 15:23:09', '1978-11-29 18:27:41', '01:06', 53, 1, 5864, 25, 22049, 5512, 27561, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12'),
(30, 7, 2, 6, 6, 'Veniam Quia Dolores Non Nemo.', 'Dolorum Fugiat Voluptas Qui Deserunt Modi Ex.', 7, 1, '1982-03-27 17:35:08', '2012-10-16 02:11:06', '07:46', 29, 1, 9524, 25, 47334, 11834, 59168, NULL, '2018-03-01 08:15:12', '2018-03-01 08:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_from` timestamp NULL DEFAULT NULL,
  `time_to` timestamp NULL DEFAULT NULL,
  `max_persons` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `info` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `date`, `name`, `location`, `time_from`, `time_to`, `max_persons`, `price`, `info`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2019-1-31', 'Eum Ducimus In Assumenda Numquam Enim Quo Nobis.', '5945 Volkman Path, Dominicton', '2019-01-31 07:54:33', '2019-01-31 10:26:49', 75, 836, 'Fuga deserunt facilis exercitationem laborum veritatis. Quo consequuntur laboriosam dolor est sit. Qui mollitia placeat dignissimos.', 'http://127.0.0.1/claes-api/storage/app/images/xphKhSha3NzEuZ63776LMF5xrE9R7x7aVy0ITSVZ.jpeg', NULL, '2018-03-01 08:15:15', '2018-04-12 00:12:05'),
(2, '1971-11-01', 'Quam Laborum Autem Explicabo Asperiores Commodi.', '888 Weimann Stream Suite 376, South Eastonstad', '2018-04-11 16:37:21', '2018-04-11 12:45:34', 34, 564, 'Tempora in odio aut voluptatem dignissimos magnam sunt. Sapiente dolorem dolorem commodi fuga praesentium dolor illo. Blanditiis dolore labore et rerum voluptatum mollitia incidunt.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\c83a582c714bd7883d408f8cea98fed8.jpg', NULL, '2018-03-01 08:15:16', '2018-03-01 08:15:16'),
(3, '2006-08-03', 'Eveniet Odit Ducimus Ducimus.', '172 Julius Green, Berryview', '2018-04-11 06:52:00', '2018-04-11 00:37:38', 11, 768, 'Aut vel nulla inventore molestiae aut illo. Nisi doloremque earum aut laudantium placeat occaecati eos. Exercitationem nesciunt et distinctio voluptatem beatae est. Inventore doloremque officiis at unde fugit.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\b0cbe031fb018c8355ed6e51daff6475.jpg', NULL, '2018-03-01 08:15:17', '2018-03-01 08:15:17'),
(4, '1972-08-01', 'Consectetur Voluptas Enim Quaerat Aut Asperiores Veritatis.', '270 Reinger Brook, Esperanzaside', '2018-04-11 17:54:49', '2018-04-11 05:37:24', 28, 854, 'Voluptatem dolore dolorem dolorem. Perferendis deserunt dolore quam corrupti molestiae omnis reiciendis.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\f54939a74e09a8741f3e72decfa1e723.jpg', NULL, '2018-03-01 08:15:19', '2018-03-01 08:15:19'),
(5, '1979-08-25', 'Neque Aut Ex Recusandae Velit Voluptates Quisquam.', '7168 McDermott Cove Apt. 138, Lake Adrien', '2018-04-10 20:39:07', '2018-04-11 02:40:29', 29, 685, 'Beatae ab cumque molestias porro. Id repellat ut tempora rerum iste numquam quaerat. Eos natus necessitatibus eos molestiae iste blanditiis a. Rem debitis ut in commodi animi.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\1d1522087772523a63f5af9fbd3e5dc2.jpg', NULL, '2018-03-01 08:15:20', '2018-03-01 08:15:20'),
(6, '1976-11-08', 'Sint Doloremque Porro Commodi In Occaecati Dolorem Provident Doloremque.', '9402 Christiana Valleys Suite 987, Koelpinland', '2018-04-10 21:09:04', '2018-04-11 11:07:16', 38, 879, 'Velit nihil odio perferendis ut consequatur aut molestias sit. Ducimus eveniet consequatur architecto sit velit amet quo. Rerum quae minus ipsa saepe.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\55afc56fdb06328978d2545b95cd67cd.jpg', NULL, '2018-03-01 08:15:21', '2018-03-01 08:15:21'),
(7, '1990-08-15', 'Assumenda Quo Dolorum Quod Quia Amet.', '7542 Sawayn Parkway Apt. 681, Elmerborough', '2018-04-11 15:33:23', '2018-04-10 21:01:13', 73, 805, 'Magni quo nemo voluptatibus facilis praesentium. Id ea ut error est porro. Quia nisi est autem.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\6eecc4622e041add34eacbf3e1727902.jpg', NULL, '2018-03-01 08:15:22', '2018-03-01 08:15:22'),
(8, '1975-05-05', 'Dolor Ut Error Id Dolorem.', '44077 Andy Land, Fernandoview', '2018-04-11 10:07:58', '2018-04-11 14:33:10', 95, 956, 'Sapiente nisi voluptates recusandae consequatur earum repudiandae accusantium. Dolor omnis corporis neque debitis porro et voluptatem. Sequi accusamus voluptate ea deserunt sequi sed architecto.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\f049ca3989279fb118c74e916e6c7ac6.jpg', NULL, '2018-03-01 08:15:23', '2018-03-01 08:15:23'),
(9, '1979-03-02', 'Numquam Explicabo Tempore Et Alias Et In.', '4660 Anastasia Union, South Mertieberg', '2018-04-11 01:34:53', '2018-04-11 12:46:54', 22, 666, 'Ea minima aut quam cupiditate impedit. Ullam ut ea quia et. Voluptatem optio magnam suscipit quo ut temporibus.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\23088a7bd44991a645b520f0d865f518.jpg', NULL, '2018-03-01 08:15:24', '2018-03-01 08:15:24'),
(10, '2002-02-10', 'Quae Alias Dolores Provident Officia Eum Ullam Fugiat.', '509 Deckow Manor Suite 727, Nonahaven', '2018-04-11 00:17:48', '2018-04-11 12:12:44', 65, 705, 'Deleniti qui reprehenderit hic quis dolores. Harum explicabo qui at assumenda. Sunt voluptatem saepe ut sed facere enim.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\5c34099f9afd68108215084eb11bb90a.jpg', NULL, '2018-03-01 08:15:25', '2018-03-01 08:15:25'),
(11, '1994-9-16', 'Deleniti Optio Ut Qui Dolor.', '331 Deon Common, West Christatown', '1994-09-16 17:24:32', '1994-09-16 07:00:22', 67, 612, 'Ut deleniti quia vero similique magni at labore molestiae. Commodi veritatis animi esse eligendi accusamus. Incidunt quasi temporibus deserunt enim.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\1520dfbcb1672762212de9a158a29a48.jpg', NULL, '2018-03-01 08:15:26', '2018-04-12 00:11:46'),
(12, '1970-04-02', 'Voluptatem Molestiae Et Hic Sit Sit Quia.', '9529 Boehm Falls, Shanahanberg', '2018-04-11 05:55:15', '2018-04-10 22:05:35', 73, 965, 'Quae voluptate sint ut aliquid exercitationem et. Perferendis tempora sequi dicta illum est. Molestiae qui voluptatem qui quod qui. Veritatis facere quia et in repellat molestiae. Sed et quia sunt voluptate quisquam a.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\5b59b8bcf120b04fa54dbc61add743a9.jpg', NULL, '2018-03-01 08:15:28', '2018-03-01 08:15:28'),
(13, '1992-08-20', 'Odit Ut Doloribus Ut Nihil.', '61768 Howe Street, Kubchester', '2018-04-10 21:43:44', '2018-04-10 20:54:25', 18, 262, 'Omnis iusto consequuntur eos molestiae corporis quo ut. Et nihil quia sequi voluptatem facere voluptatum. Omnis sequi quia porro et.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\18ab2d53d1fdf8ca206ddb152c55eb38.jpg', NULL, '2018-03-01 08:15:29', '2018-03-01 08:15:29'),
(14, '1992-07-16', 'Qui Adipisci Perferendis Porro Omnis Consequatur Sint Quaerat.', '810 Champlin Walks Suite 785, North Estelle', '2018-04-11 00:33:50', '2018-04-10 21:54:33', 81, 224, 'Sed quia laborum cupiditate fugiat. Et consequatur quisquam enim quasi delectus. Provident officia itaque sint animi eum nobis rerum.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\7c140e983a62f8c102fb6c41176a0a3c.jpg', NULL, '2018-03-01 08:15:30', '2018-03-01 08:15:30'),
(15, '1991-04-27', 'Omnis Et Ullam Aperiam Dignissimos Recusandae Temporibus.', '4882 Amaya Shores, Russelview', '2018-04-11 13:49:08', '2018-04-11 07:18:09', 82, 768, 'Iusto amet fugiat odit et tenetur veniam aliquam quasi. Necessitatibus quo magnam animi eos aliquam. Esse consequatur est quidem nam. Quam enim placeat dolorum voluptatibus sed nihil.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\688239bccd12c41ebe968424c3d2c810.jpg', NULL, '2018-03-01 08:15:31', '2018-03-01 08:15:31'),
(16, '1980-11-20', 'Impedit Et Ut Id Tenetur Omnis.', '993 Ruecker Trafficway, North Marjolainehaven', '2018-04-10 20:36:41', '2018-04-10 22:08:32', 71, 367, 'Iste culpa occaecati hic blanditiis non pariatur animi. Id dolore nesciunt asperiores et est. Minima consequatur et culpa rerum accusantium aut temporibus. Maxime dolores enim dolorem culpa.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\3c026b07567d9c84c2bbf8099416ed2e.jpg', NULL, '2018-03-01 08:15:33', '2018-03-01 08:15:33'),
(17, '2010-08-14', 'Deserunt Id Doloribus Officiis Aliquam.', '2305 Collins Road, Yvonnemouth', '2018-04-11 18:00:58', '2018-04-11 16:28:30', 0, 149, 'Officiis corrupti reiciendis tenetur quo aliquam. Provident quis quisquam ut dolorum officiis. Vel cumque ab et. Harum error tempora ut qui quia ut placeat.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\01eb72de923f2d9de308a91f2bd97c56.jpg', NULL, '2018-03-01 08:15:34', '2018-03-01 08:15:34'),
(18, '2009-04-30', 'Enim Ratione Hic Quia Dolorem Ut Ad Eveniet.', '485 Fisher Camp Apt. 134, Lake Judson', '2018-04-11 12:59:44', '2018-04-11 07:50:07', 45, 156, 'Assumenda vero accusamus nisi quasi. Consectetur pariatur assumenda perspiciatis incidunt temporibus. Laudantium est tempora delectus totam. Porro rerum accusantium reprehenderit aut nihil quos.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\2f2ad46e1bb06f24f1f253cd8c5943cc.jpg', NULL, '2018-03-01 08:15:35', '2018-03-01 08:15:35'),
(19, '2016-09-30', 'Aperiam Ea Provident Non Totam Sequi.', '83399 O\'Kon Spring, Jacobsport', '2018-04-10 21:24:39', '2018-04-11 07:08:06', 71, 597, 'Vitae reprehenderit commodi incidunt nulla sed nobis. Recusandae et nemo dolores. Ullam adipisci quod et et ipsa modi maiores ullam.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\90b19efa1b7dae6db02f123be85aca4e.jpg', NULL, '2018-03-01 08:15:36', '2018-03-01 08:15:36'),
(20, '1996-03-25', 'Sit Nisi Vitae Suscipit Consequuntur Sint.', '81320 Judah Springs Suite 929, Schowalterstad', '2018-04-11 06:49:08', '2018-04-11 04:15:23', 67, 404, 'Quam delectus modi tenetur molestias sequi. Aut cupiditate voluptatem odio. Neque repudiandae asperiores ad praesentium. Excepturi ipsum animi omnis voluptatem officia et.', 'C:\\Users\\jasma\\AppData\\Local\\Temp\\634b8d27a7f9b51813b022dc12df66c9.jpg', NULL, '2018-03-01 08:15:37', '2018-03-01 08:15:37'),
(21, '2018-10-03', 'test event edit', 'Mohali edit', '2018-04-11 04:30:00', '2018-04-11 04:30:00', 3, 100, 'testing info edit', 'http://127.0.0.1/claes-api/storage/app/images/ZzwQFv8dDUzk6erwVgbTzIN1ba5e3nsnYJu0wINy.jpeg', '2018-03-30 12:34:25', '2018-03-30 07:04:25', '2018-03-31 05:09:10'),
(24, '2018-10-03', 'testing event', 'chandigarh', '2018-04-10 18:30:00', '2018-04-10 18:30:00', 10, 100, 'testing', 'http://127.0.0.1/claes-api/storage/app/images/lELfS4TgG14B07O5Fcw1JoHwbUIx6aWKhgRR7Zvk.jpeg', '2018-04-05 02:35:16', '2018-04-04 21:05:16', '2018-04-11 00:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `mackerias`
--

CREATE TABLE `mackerias` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `delivery_date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivered` tinyint(4) NOT NULL DEFAULT '0',
  `info` text COLLATE utf8mb4_unicode_ci,
  `total` int(11) NOT NULL DEFAULT '0',
  `vat_total` int(11) NOT NULL DEFAULT '0',
  `total_incl_vat` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mackerias`
--

INSERT INTO `mackerias` (`id`, `date`, `customer_id`, `contact_id`, `delivery_date`, `delivered`, `info`, `total`, `vat_total`, `total_incl_vat`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1973-10-09', 1, 20, '2018-5-12', 1, 'Voluptates molestias velit qui recusandae. Et harum molestiae corporis. Corporis officiis ea nulla quia rerum doloremque occaecati minus.', 111052, 27763, 138815, NULL, '2018-03-01 08:15:39', '2018-04-11 04:57:47'),
(2, '1972-02-13', 1, 9, '2004-04-29', 1, 'Autem dicta doloribus et cum cumque facere pariatur qui. Magni maiores consequuntur distinctio et qui. Dolores amet dolorem voluptatem voluptatibus.', 9080, 2270, 11350, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(3, '2013-09-20', 1, 4, '1982-04-11', 1, 'Sit officia provident voluptatem. Assumenda aut dolor ab tenetur nihil non. Exercitationem perspiciatis voluptatum nobis voluptas est quo nesciunt dolore. Sapiente quis earum et deleniti vero distinctio.', 1301, 325, 1626, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(4, '1985-06-14', 2, 9, '1975-04-28', 1, 'Qui et est autem harum molestiae. Ullam non debitis fugiat velit tenetur modi illo. Ut nihil ducimus laborum fugit doloribus dolor. Nihil et eum quis a praesentium.', 7108, 1777, 8885, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(5, '1979-09-01', 2, 9, '1981-08-20', 0, 'Quia consequuntur iusto fuga illo quia laborum voluptatem repellendus. Recusandae quidem voluptatem numquam quibusdam recusandae molestiae. Est est est corporis nesciunt aut autem quia. Consequuntur neque voluptatum distinctio nobis aut sed.', 6492, 1623, 8115, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(6, '1990-10-19', 2, 0, '2004-10-28', 1, 'Rerum et doloremque modi. Iure vel non exercitationem mollitia. Maiores voluptatum inventore iure et cumque ut tempora inventore.', 7739, 1935, 9674, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(7, '2016-09-19', 3, 5, '1984-05-26', 1, 'Quibusdam ipsa id doloremque modi odio. Quisquam exercitationem et quis et est rerum. Assumenda debitis beatae explicabo consequatur ad ducimus deleniti explicabo.', 2558, 640, 3198, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(8, '2002-02-21', 3, 5, '2004-03-25', 0, 'Maxime est non cum sunt et ab non. Quo eaque et quia asperiores ab voluptate nemo. Dolorem voluptates consequatur est nesciunt sed adipisci qui. Fugiat nihil sint nulla voluptas explicabo.', 2789, 697, 3486, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(9, '1985-07-21', 3, 4, '2007-06-11', 0, 'Voluptatem nulla est quis incidunt autem. Libero aliquam quae sint. Est nobis aut doloribus molestiae odit et ab. Consequatur sint quibusdam aperiam et molestiae quisquam.', 6865, 1716, 8581, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(10, '1978-06-03', 3, 5, '2013-01-01', 1, 'Perferendis magni voluptatem sit et reiciendis qui nam. Voluptatibus dolore ipsam fugiat quia. Ut modi et velit consectetur natus eaque voluptatem.', 6358, 1590, 7948, NULL, '2018-03-01 08:15:40', '2018-03-01 08:15:40'),
(14, NULL, 2, 4, '2018-03-18', 1, 'testing', 3390, 848, 4238, '2018-04-04 10:00:49', '2018-04-04 04:30:49', '2018-04-04 04:31:42'),
(15, NULL, 2, 20, '2018-03-20', 0, 'testing info edit', 562, 141, 703, '2018-04-05 02:45:56', '2018-04-04 21:15:56', '2018-04-11 03:39:30');

-- --------------------------------------------------------

--
-- Table structure for table `mackeria_articles`
--

CREATE TABLE `mackeria_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `mackeria_id` int(11) NOT NULL DEFAULT '0',
  `article_id` int(11) NOT NULL DEFAULT '0',
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  `vat_total` int(11) NOT NULL DEFAULT '0',
  `total_incl_vat` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mackeria_articles`
--

INSERT INTO `mackeria_articles` (`id`, `mackeria_id`, `article_id`, `date`, `quantity`, `name`, `price`, `total`, `vat_total`, `total_incl_vat`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 3, '1995-01-16', 5, 'Assumenda Omnis Et Ut Temporibus.', 1480, 7400, 1850, 9250, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(2, 6, 1, '1984-09-06', 1, 'Voluptas Asperiores Quos Officiis Non Occaecati Ipsum Dolores.', 1570, 1570, 392, 1962, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(3, 4, 5, '1978-10-17', 2, 'Ut Ratione Praesentium Beatae Qui Dolorem Nostrum Architecto.', 9454, 18908, 4727, 23635, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(4, 2, 5, '1974-04-15', 5, 'Animi Illo Soluta Accusamus Iusto Vitae Dolor.', 3787, 18935, 4734, 23669, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(5, 5, 9, '2010-03-09', 6, 'Est Ullam Recusandae Cumque Dolor Tenetur.', 4368, 26208, 6552, 32760, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(6, 4, 3, '1970-12-27', 6, 'Illum Corrupti Soluta Suscipit Quia Itaque Porro.', 4731, 28386, 7096, 35482, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(7, 4, 9, '2013-09-22', 2, 'Doloribus Voluptatibus Sapiente Architecto Nihil Quam Commodi.', 4921, 9842, 2460, 12302, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(8, 2, 3, '1995-07-09', 3, 'Et Numquam Beatae Est Optio Sapiente Pariatur.', 809, 2427, 607, 3034, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(9, 3, 1, '1994-12-17', 1, 'Adipisci Molestiae Illum Et Omnis Autem Debitis.', 6995, 6995, 1749, 8744, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(10, 8, 6, '1994-10-25', 0, 'Suscipit Dolores Voluptatem Numquam Ut Pariatur Labore Eaque.', 7417, 0, 0, 0, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(11, 2, 2, '1978-10-16', 9, 'Esse Nostrum Deserunt Aperiam At Soluta.', 7887, 70983, 17746, 88729, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(12, 8, 0, '1987-07-02', 7, 'Alias Illum Eos Quo Omnis Maiores Sit Error.', 5368, 37576, 9394, 46970, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(13, 3, 7, '2012-04-27', 5, 'A Tempore In Amet Eos Odit Suscipit Ut.', 6828, 34140, 8535, 42675, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(14, 1, 9, '1989-03-10', 9, 'Consequatur Repudiandae Enim Quisquam Aut.', 9268, 83412, 20853, 104265, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(15, 0, 5, '1976-07-24', 1, 'Qui Dolorem Impedit Reprehenderit Perspiciatis Natus.', 5110, 5110, 1278, 6388, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(16, 0, 3, '1979-10-25', 3, 'Voluptatem Consequatur Fugiat Eaque Quisquam Fuga.', 6629, 19887, 4972, 24859, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(17, 1, 0, '1994-10-21', 2, 'Totam Non Quas Quam Eaque Ut Porro Eos.', 8573, 17146, 4286, 21432, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(18, 8, 2, '2006-09-03', 2, 'Nesciunt Ducimus Et Illo Assumenda Eum Dolorem Ipsam.', 980, 1960, 490, 2450, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(19, 9, 8, '1972-06-06', 7, 'Aut Amet Saepe Optio Atque Magnam Optio Exercitationem.', 3446, 24122, 6030, 30152, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(20, 5, 5, '1972-01-20', 6, 'Ullam Voluptas Et Expedita Et.', 8780, 52680, 13170, 65850, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(21, 9, 4, '2007-02-09', 2, 'Delectus Delectus Consectetur Veritatis Impedit Id Blanditiis.', 2775, 5550, 1388, 6938, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(22, 9, 9, '2003-04-07', 2, 'At Fuga Dolorem Qui Quod.', 8278, 16556, 4139, 20695, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(23, 1, 3, '1971-10-03', 4, 'Vel Dolorem Est Aut Blanditiis Non.', 1785, 7140, 1785, 8925, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(24, 0, 3, '1982-06-26', 4, 'Ut Ab Atque Ducimus Odit.', 1483, 5932, 1483, 7415, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(25, 6, 4, '1970-02-10', 1, 'Similique Omnis Facilis Numquam Inventore Occaecati Facilis Fugiat.', 8528, 8528, 2132, 10660, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(26, 7, 6, '1982-01-22', 6, 'Maxime Ut Numquam Dolorem Enim Temporibus.', 250, 1500, 375, 1875, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(27, 9, 6, '1971-08-12', 0, 'Necessitatibus Aut Sunt Veritatis Eius Sint.', 9066, 0, 0, 0, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(28, 0, 6, '1979-03-03', 5, 'Quos Deserunt Omnis Earum Enim Cumque Veniam Vero Itaque.', 2364, 11820, 2955, 14775, NULL, '2018-03-01 08:15:38', '2018-03-01 08:15:38'),
(29, 0, 8, '1982-04-25', 0, 'Ex Temporibus Ut Sapiente Aut Velit.', 1842, 0, 0, 0, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(30, 0, 1, '2004-03-23', 9, 'Et Eaque Omnis Dolores Voluptatem Explicabo Aut.', 9026, 81234, 20308, 101542, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(31, 5, 4, '1974-06-05', 7, 'Quam Consequatur Eos Suscipit Et Vero Nisi.', 9712, 67984, 16996, 84980, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(32, 0, 0, '1991-12-05', 6, 'Eveniet Quos Et Sed Voluptas Aut Rem.', 1080, 6480, 1620, 8100, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(33, 9, 7, '2007-02-24', 0, 'Hic Nobis Consequuntur Suscipit.', 3592, 0, 0, 0, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(34, 2, 5, '1998-11-29', 6, 'Aliquam Aut Error Nulla Sed Blanditiis Totam Tempora Aliquam.', 4871, 29226, 7306, 36532, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(35, 9, 5, '1980-10-29', 0, 'Autem Soluta Minima Sint Tempora Quo.', 5746, 0, 0, 0, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(36, 8, 3, '1986-01-06', 7, 'Aliquid Ea Fugit Reprehenderit Possimus Minima Dicta.', 6288, 44016, 11004, 55020, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(37, 4, 3, '2016-05-28', 8, 'Occaecati Minima Suscipit Id Sequi.', 2049, 16392, 4098, 20490, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(38, 4, 8, '1989-01-07', 5, 'Odio Enim Sequi Molestias Minima Sed Ut.', 307, 1535, 384, 1919, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(39, 8, 9, '1999-07-02', 7, 'Adipisci Molestias Iure Vel Enim Quae Tempore.', 9005, 63035, 15759, 78794, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(40, 2, 1, '2003-05-01', 0, 'Non Ullam Fugiat Eum Rerum.', 5969, 0, 0, 0, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(41, 7, 4, '1986-03-26', 7, 'Quo Non Necessitatibus Ut Quam Dolore In Et Eligendi.', 695, 4865, 1216, 6081, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(42, 7, 3, '2009-10-06', 5, 'Illum In Aut Dolores Qui Beatae Aut.', 769, 3845, 961, 4806, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(43, 1, 4, '1985-01-11', 3, 'Voluptatibus Nisi Ut Iusto Dolorem Architecto Molestiae.', 1118, 3354, 838, 4192, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(44, 3, 9, '1977-05-09', 5, 'Praesentium Ut Qui Id Quisquam Dolore Beatae.', 3027, 15135, 3784, 18919, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(45, 2, 8, '1973-07-18', 2, 'Non Voluptatem Qui Labore Sequi Sed Harum.', 6111, 12222, 3056, 15278, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(46, 6, 2, '1989-11-26', 3, 'Nemo Deserunt Quia Velit Ex.', 3837, 11511, 2878, 14389, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(47, 3, 4, '1983-05-07', 9, 'Qui Et Aut Modi Eos Quae Unde.', 9406, 84654, 21164, 105818, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(48, 9, 3, '1997-10-24', 2, 'Qui Commodi Blanditiis Quia.', 8558, 17116, 4279, 21395, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(49, 9, 7, '1997-07-29', 3, 'Voluptatem Aliquam Voluptatem Recusandae Sapiente Qui Ut.', 7623, 22869, 5717, 28586, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(50, 8, 0, '1999-12-11', 5, 'Sint Est Sit Consequatur Neque Itaque Ex Ut.', 678, 3390, 848, 4238, NULL, '2018-03-01 08:15:39', '2018-03-01 08:15:39'),
(64, 13, 1, '1991-11-27', 6, 'Ex consectetur accusantium angular', 678, 4068, 1017, 5085, '2018-04-04 09:03:31', '2018-04-04 09:03:31', '2018-04-04 09:03:31'),
(65, 14, 1, '1991-11-27', 5, 'Ex consectetur accusantium angular', 678, 3390, 848, 4238, '2018-04-04 10:01:36', '2018-04-04 10:01:36', '2018-04-04 10:01:36'),
(78, 15, 4, '2006-01-13', 1, 'test edit 4', 562, 562, 140, 702, '2018-04-10 18:25:00', '2018-04-10 18:25:00', '2018-04-10 18:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(21, '2014_10_12_000002_create_users_table', 1),
(22, '2014_10_12_100000_create_password_resets_table', 1),
(23, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(24, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(25, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(26, '2016_06_01_000004_create_oauth_clients_table', 1),
(27, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(28, '2018_01_18_032724_create_articles_table', 1),
(29, '2018_01_18_052950_create_customers_table', 1),
(30, '2018_01_18_061417_create_events_table', 1),
(31, '2018_02_09_051513_create_article_groups_table', 1),
(32, '2018_02_14_003623_create_economies_table', 1),
(33, '2018_02_14_005532_create_economy_articles_table', 1),
(34, '2018_02_14_011519_create_mackerias_table', 1),
(35, '2018_02_14_035950_create_mackeria_articles_table', 1),
(36, '2018_02_14_041135_create_partners_table', 1),
(37, '2018_02_14_041740_create_bookings_table', 1),
(38, '2018_02_14_042135_create_package_articles_table', 1),
(39, '2018_02_14_042635_create_settings_table', 1),
(40, '2018_02_14_043440_create_participants_table', 1),
(42, '2018_04_09_173224_set_articles_dates_to_current_timestamp', 2),
(43, '2018_04_09_173505_set_articlesgroups_dates_to_current_timestamp', 3),
(44, '2018_04_09_173630_set_packagearticles_dates_to_current_timestamp', 4),
(45, '2018_04_11_124208_change_time_fields_datatype', 5),
(46, '2018_04_11_172225_change_events_fields_datatype', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('02ec60b5c353343078a372f88aa5051d2867f22505c518a03e42232f8834e4167e2879a68101d5fe', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-02 02:55:15', '2018-03-02 02:55:15', '2019-03-02 08:25:15'),
('034e7824c5a7145ce474b83ab7d4c25295c7729f22487973ec5fdbf4f5946f653b8776ed596a79eb', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 07:41:33', '2018-03-16 07:41:33', '2019-03-16 13:11:33'),
('1a1c2c063177dc74b8fb088071a92cd3f37f3b13254be66b33fcdb0cb680da5703db4fedbe782f74', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-02 03:03:43', '2018-03-02 03:03:43', '2019-03-02 08:33:43'),
('2b2a148a62134186de472f24ed8a87aa66dc14a44cd8933785e9241a888231db2778058c79893037', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 07:41:58', '2018-03-16 07:41:58', '2019-03-16 13:11:58'),
('58a2d2124a792bdf31e4420a7769ca3b7f4d81e44b2182e1d31c8af5ceb97d6b6701182791b9b13f', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 06:42:40', '2018-03-16 06:42:40', '2019-03-16 12:12:40'),
('62e8e0e8d612eaa7bacae92f262ad228d7914c21e74fc391ba155e1600e37cc8d08394d0fc41d8d7', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-20 05:30:10', '2018-03-20 05:30:10', '2019-03-20 11:00:10'),
('6a2f8aed075415a74ca13cb13b9422bcbc416599fd5dc300c57e5e679627b7d5c37fee07adca550b', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 07:38:44', '2018-03-16 07:38:44', '2019-03-16 13:08:44'),
('738020fc7cb98f5f5e6e0a173708d17bef4f6d3033c48eb15947373c5169f451dc49c8dda2388319', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-20 04:52:43', '2018-03-20 04:52:43', '2019-03-20 10:22:43'),
('75d5af1d37c3fb1db0b257e16efa975a3f24c26223b9ff7f106e2a2a5700a9f132b7ce1eea3d4c64', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-20 04:54:57', '2018-03-20 04:54:57', '2019-03-20 10:24:57'),
('78527ce85382ed3d92103516e569f48aeebba77456e25298c89dfafa8bc09a7c7c989534e998174b', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-19 04:26:30', '2018-03-19 04:26:30', '2019-03-19 09:56:30'),
('7b6c3f1effbcc2912bd6d777c56471da8c93da4ee04b5a1621da9f2057602941d20528d39f1e0460', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-26 11:45:40', '2018-03-26 11:45:40', '2019-03-26 17:15:40'),
('841838c77b7a08542c399f279db969d7cc65059533a0c77b22717b1d5bcb013254490793eb83ed88', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-20 04:50:21', '2018-03-20 04:50:21', '2019-03-20 10:20:21'),
('897d48efb9588db1ea428d29363a911c1bc905d813c7f0a75626ecb7bd253f49b7b9cb3b4df7a250', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 07:37:59', '2018-03-16 07:37:59', '2019-03-16 13:07:59'),
('ab706f4ad5dbcbfacfca0d88f95a346bd7e02f681bf53793383724887a21f9b46005a9e95edd4abe', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-19 07:24:15', '2018-03-19 07:24:15', '2019-03-19 12:54:15'),
('ac76e42d50da2da622d098aac13422fe967dfb8cfed223280ee7c94ea361e9a21e248cf192ae5f9e', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-02 03:27:08', '2018-03-02 03:27:08', '2019-03-02 08:57:08'),
('acb62b97e2977650239fb669c748c3ad45cae94471bbc8378bfa767fa4b09fb51f4a406e4f32744c', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-20 04:49:41', '2018-03-20 04:49:41', '2019-03-20 10:19:41'),
('ba141f48908b70226837380cb9a0db8a53fe13b2fc374a0966ceac6412cc74de8ec52abee95d2fdb', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 07:39:21', '2018-03-16 07:39:21', '2019-03-16 13:09:21'),
('d2c44f681d946861ce7e186014c59bacffcdcf2dcf62eda5acd93868de270bb7ea5bec22841802da', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 07:37:45', '2018-03-16 07:37:45', '2019-03-16 13:07:45'),
('d3ca0770ffb78c9d158111f57b6911de24f1d729c0257316ec8061942ae8365af1aadfe5753f110b', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-16 07:23:38', '2018-03-16 07:23:38', '2019-03-16 12:53:38'),
('de388e7cb4fc4f04aefa3aa1b26410f8ba43d731964891e6d744d49aeb03685bd8100af3e8693580', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-19 07:24:35', '2018-03-19 07:24:35', '2019-03-19 12:54:35'),
('f487c4cae6e721d279c86c2d8be0a83f20bdfe1569c9ac0951cca17e340cab0f36f96e3cc8edb278', 2, 1, 'FrodoAPI', '[]', 0, '2018-03-20 04:54:33', '2018-03-20 04:54:33', '2019-03-20 10:24:33');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'FrodoAPI Personal Access Client', 'kczYPVL9iGsop9qyLYzbiZLmQslTlP16tUEiHjDr', 'http://localhost', 1, 0, 0, '2018-03-02 02:54:58', '2018-03-02 02:54:58'),
(2, NULL, 'FrodoAPI Password Grant Client', 'X4ZtbJyb5zW0InYKisQYGBa3HUvoOWmbvCbEe9Dl', 'http://localhost', 0, 1, 0, '2018-03-02 02:54:58', '2018-03-02 02:54:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-03-02 02:54:58', '2018-03-02 02:54:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_articles`
--

CREATE TABLE `package_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_articles`
--

INSERT INTO `package_articles` (`id`, `article_id`, `package_id`, `created_at`, `updated_at`) VALUES
(1, 8, 5, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(2, 1, 0, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(3, 1, 2, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(4, 7, 4, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(5, 7, 0, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(6, 0, 5, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(7, 1, 2, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(8, 4, 5, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(9, 2, 9, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(10, 5, 3, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(11, 5, 7, '2018-03-01 13:45:40', '2018-03-01 13:45:40'),
(12, 9, 0, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(13, 4, 3, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(14, 7, 6, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(15, 6, 5, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(16, 6, 4, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(17, 0, 2, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(18, 7, 4, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(19, 7, 7, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(20, 2, 2, '2018-03-01 13:45:41', '2018-03-01 13:45:41'),
(35, 42, 1, '2018-04-05 02:31:47', '2018-04-05 02:31:47'),
(36, 42, 4, '2018-04-05 02:31:47', '2018-04-05 02:31:47'),
(38, 11, 4, '2018-04-09 11:39:25', '2018-04-09 11:39:25'),
(39, 11, 1, '2018-04-09 11:41:05', '2018-04-09 11:41:05'),
(40, 12, 1, '2018-04-09 13:02:36', '2018-04-09 13:02:36'),
(41, 14, 1, '2018-04-11 07:16:21', '2018-04-11 07:16:21'),
(42, 14, 4, '2018-04-11 07:25:51', '2018-04-11 07:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `date`, `name`, `company`, `mobile`, `email`, `event_id`, `created_at`, `updated_at`) VALUES
(1, '1976-10-26', 'Abe Lowe', 'Kuhlman and Sons', '+8891705875531', 'connelly.lydia@gmail.com', 5, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(2, '2006-06-24', 'Amaya Wyman', 'Davis, Murray and Glover', '+1333629309594', 'flockman@gmail.com', 6, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(3, '1987-07-02', 'Mack Frami', 'Rodriguez-Hilpert', '+6313040775423', 'xbauch@yahoo.com', 7, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(4, '2013-01-03', 'Mr. Deon Luettgen II', 'Jacobi Inc', '+6189811473234', 'avis89@yahoo.com', 8, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(5, '1986-01-21', 'Alexandrea Ortiz', 'Oberbrunner LLC', '+8913411622150', 'becker.liliana@gleason.biz', 5, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(6, '1977-01-06', 'Joseph Turner III', 'Buckridge, Howe and Macejkovic', '+8264587453895', 'jovani41@hotmail.com', 1, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(7, '1995-12-21', 'Trace Emmerich DDS', 'Swaniawski-Wolf', '+9257960940161', 'bblanda@hotmail.com', 8, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(8, '1994-04-07', 'Ms. Tiana Auer PhD', 'Pouros-Gleason', '+1481109831995', 'pmoen@hagenes.com', 7, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(9, '1978-07-12', 'Maverick Fritsch', 'Wunsch-Terry', '+9459613647804', 'hsimonis@hotmail.com', 9, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(10, '1994-10-20', 'Ivory Larson IV', 'Runte-Nienow', '+7714003722019', 'bmorar@ward.com', 3, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(11, '1978-03-16', 'Pink Rolfson', 'Kuhn, Erdman and Collins', '+8426847591066', 'weimann.zackary@yahoo.com', 7, '2018-03-01 08:15:41', '2018-03-01 08:15:41'),
(13, '1986-05-12', 'Prof. Emile Douglas', 'Heller-Rice', '+9218989074643', 'crunolfsson@hotmail.com', 2, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(14, '2010-12-10', 'Giovani McLaughlin', 'Bailey-Stark', '+3144725264129', 'erempel@fahey.com', 2, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(15, '1997-09-07', 'Kayden Dickens', 'Mante, Reilly and Mitchell', '+2726556122044', 'pouros.kyra@lockman.com', 6, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(16, '1980-05-06', 'Narciso McClure Jr.', 'Crona LLC', '+5003151037402', 'xswaniawski@fahey.com', 4, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(17, '2013-06-25', 'Miss Cheyenne Auer DVM', 'Abernathy Ltd', '+4395780264096', 'tressa19@yahoo.com', 0, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(18, '1994-08-10', 'Felipa Wunsch', 'Moore Group', '+8987613032893', 'ritchie.pearlie@gmail.com', 7, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(19, '1996-09-02', 'Julie Thiel', 'Wilkinson-Labadie', '+4108339229584', 'schoen.morton@rosenbaum.info', 2, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(20, '2005-08-04', 'Alex Rodriguez', 'Luettgen-Lynch', '+9091316474016', 'ciara.franecki@nikolaus.com', 9, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(21, '1991-07-18', 'Jillian Feeney', 'Hackett, Emard and Lebsack', '+3608108910832', 'coty40@kunze.biz', 8, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(22, '1998-01-04', 'Prof. Donnell Sanford V', 'Williamson Ltd', '+4441581550856', 'wwintheiser@gmail.com', 6, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(23, '2003-10-06', 'Dr. Juana Greenholt', 'Doyle Inc', '+7478232119838', 'mtorp@kihn.info', 8, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(24, '2007-09-08', 'Stanford Eichmann', 'Greenfelder Ltd', '+2552707258271', 'martina.cole@bosco.com', 5, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(25, '1977-12-12', 'Chanel Jacobson', 'Thiel Group', '+3693513858996', 'unienow@gmail.com', 4, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(26, '2008-08-02', 'Avis Rowe', 'Runolfsson LLC', '+3768936709352', 'luettgen.amya@schimmel.com', 0, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(27, '2005-11-05', 'Richmond Lynch', 'Beier, Leannon and Wisoky', '+7638162392989', 'jayda23@yahoo.com', 9, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(28, '1994-09-16', 'Ms. Maeve Hackett', 'Borer, Schimmel and Cronin', '+3776169768857', 'gustave.howell@wehner.biz', 0, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(29, '2008-06-11', 'Naomie Conn', 'Prohaska PLC', '+3828996556076', 'mckenzie.roxane@sauer.com', 3, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(30, '1993-02-28', 'Cathryn Daugherty', 'Schumm Inc', '+1539454013623', 'mariela58@becker.biz', 5, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(31, '2014-05-16', 'Prof. Ladarius Beer', 'Paucek, Hermann and Gleason', '+9176380658115', 'ariel90@nikolaus.com', 5, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(32, '1977-03-31', 'Kenneth Greenholt', 'Leuschke Inc', '+5374654459655', 'alexa70@rau.com', 5, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(33, '2001-07-05', 'Prof. Jaylon Toy DDS', 'Pouros, Reinger and Doyle', '+3188578167805', 'johnston.dahlia@yahoo.com', 4, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(34, '1994-11-22', 'Janis Shields', 'Windler, Smitham and Hilll', '+2376687378578', 'ellis.oconner@baumbach.biz', 8, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(35, '1986-07-11', 'Dorris Beer Jr.', 'Roob-Barrows', '+6208308524518', 'mabel29@tremblay.com', 3, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(36, '1998-11-16', 'Freddie D\'Amore I', 'Reichel, Nitzsche and Goodwin', '+5635604382845', 'arlo96@hotmail.com', 3, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(37, '1997-04-23', 'Lance Hamill', 'Cartwright PLC', '+3275279698581', 'collin56@yahoo.com', 2, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(39, '1972-03-10', 'Reggie Schumm Jr.', 'McGlynn-Fisher', '+5638672839277', 'wbarrows@konopelski.com', 6, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(40, '2009-06-18', 'Ignacio Greenholt III', 'Ebert, Grimes and Bernhard', '+7141537473551', 'metz.haleigh@weissnat.com', 3, '2018-03-01 08:15:42', '2018-03-01 08:15:42'),
(41, '1984-02-02', 'Logan Littel', 'Bergstrom, Ryan and Wiza', '+8318841398966', 'ole.fritsch@yahoo.com', 8, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(42, '2008-11-25', 'Saul Dietrich I', 'Kertzmann, Prohaska and Klein', '+5608759304336', 'lavonne32@yahoo.com', 6, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(43, '1976-06-23', 'Mireya Ferry', 'Marquardt, Koss and Hirthe', '+5731644077450', 'vwitting@tillman.com', 6, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(44, '2001-06-29', 'Tod Kutch', 'Lubowitz-Becker', '+7572478050721', 'baron.terry@kozey.com', 4, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(45, '1984-05-01', 'Chadd Hyatt', 'O\'Kon and Sons', '+6694063685396', 'pmann@gmail.com', 6, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(46, '2001-01-04', 'Virgil Lockman III', 'Hammes Group', '+2500684507330', 'earnestine91@kerluke.biz', 9, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(47, '2010-05-30', 'Cristian Sipes', 'Swift LLC', '+5885324987569', 'strosin.nolan@king.com', 9, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(48, '1977-12-15', 'Kylie DuBuque', 'Ortiz-Daugherty', '+8822317981467', 'dupton@yahoo.com', 0, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(49, '1979-08-02', 'Skylar Weimann', 'Rowe, Dickinson and Denesik', '+6528530838944', 'cordelia59@hotmail.com', 9, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(51, '1976-10-26', 'Abe Lowe', 'Kuhlman and Sons', '+8891705875531', 'connelly.lydia@gmail.com', 24, '2018-03-01 08:15:41', '2018-03-01 08:15:41');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `function` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `date`, `company`, `contact`, `function`, `phone`, `mobile`, `email`, `info`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1974-09-17', 'Nikolaus Inc', 'Blake Schneider', 'Quas Saepe Consequuntur Exercitationem Est Dolorem Sit.', '+2541477087508', '+4477039700455', 'info@magnam.com', 'Eum dolore et placeat deleniti dolorem. Dolor aliquam sint molestiae minus porro architecto dolorem. Fuga qui alias quis dolor.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(2, '1988-06-21', 'Braun-Feest', 'Scot White', 'Ut Corporis Id Numquam Ratione Cumque Aut.', '+6941980912332', '+5721443027235', 'info@totam.com', 'Id eum officia est exercitationem nam. Ipsa perferendis et veniam molestiae quaerat quibusdam. Dolores maiores architecto adipisci occaecati sapiente dolorum consequatur.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(3, '1982-02-02', 'Mertz-Towne', 'Delores Legros Sr.', 'Ea Quas Praesentium Ex Dolores.', '+4267401558780', '+3951512495008', 'info@quis.com', 'Ut quaerat numquam iure. Omnis dicta sed modi quisquam. Iste qui dolor architecto accusantium ex.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(4, '1986-11-26', 'Ritchie, Pfannerstill and Bradtke', 'Cornell Gottlieb', 'Voluptatibus Quas Eius Possimus Doloribus Unde Assumenda Veniam.', '+3394022358714', '+8036254680312', 'info@cumque.com', 'Corporis ut reiciendis sit ad tenetur hic sed. Consequatur tenetur corporis explicabo explicabo tenetur. Nihil velit vel natus nihil est. Et autem consequatur et necessitatibus eaque veniam illo.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(5, '2013-02-15', 'Yost-Sporer', 'Carter Gislason', 'Sed Natus Consequatur Nihil Officia.', '+4490038473442', '+2170733033533', 'info@dolorum.com', 'Aspernatur fuga id sed. Ex enim porro in qui consectetur aliquid qui modi. Molestiae vel non qui quam.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(6, '2003-06-29', 'Ankunding, Lemke and Ryan', 'Prof. Brook Hauck DDS', 'Sit Architecto Dolore Ut Optio Impedit Qui.', '+1543733851816', '+3403590796419', 'info@sunt.com', 'Aliquid cum iste blanditiis voluptatibus voluptatibus. Accusantium nobis iure quisquam molestias asperiores voluptatibus possimus. Commodi aspernatur molestiae natus est dolore fuga neque alias.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(7, '2003-06-06', 'Rice Ltd', 'Dasia Gerlach', 'Nesciunt Autem Occaecati Et Id Consequatur Tempore Dolore.', '+6178973389922', '+5632246412690', 'info@voluptas.com', 'Iusto ut voluptatibus ipsum eligendi dolore animi. Et expedita optio et dolorum nostrum sit reprehenderit corporis. Placeat dolorem veritatis rem consequatur. Magni sunt aliquam officiis qui id.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(8, '1995-12-11', 'Schneider Group', 'Justice Kemmer', 'Vitae Iste Quia Sunt Deleniti Quas.', '+7721973257421', '+7127620273414', 'info@nulla.com', 'Non ut vel ea enim tempora reiciendis. Consequatur distinctio aspernatur sequi ad sit. Quos voluptatem dolore consequuntur enim.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(9, '2008-01-04', 'Bahringer, Dicki and Flatley', 'Mr. Olin Pouros DVM', 'Est Maxime Eos Voluptatem.', '+5445676308521', '+3849467856572', 'info@voluptatem.com', 'Quia labore voluptatem perspiciatis. Maiores adipisci nisi doloribus enim voluptatibus dolorem voluptatem molestiae. Nemo veniam debitis pariatur quasi et eum.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43'),
(10, '2018-02-08', 'Hettinger Inc', 'Alden Thompson', 'Quia Sapiente Cupiditate Et Animi Voluptate Sint Nihil.', '+6520867783694', '+4951316451965', 'info@nihil.com', 'Sed vero iusto et repellendus placeat sapiente. Nihil unde odio aut repellat.', NULL, '2018-03-01 08:15:43', '2018-03-01 08:15:43');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Units', '[\"Styck\",\"Timme\",\"Dag\",\"Månad\"]', '2018-03-01 08:15:44', '2018-03-01 08:15:44'),
(2, 'Result Units', '[\"Kontorshotell\",\"Konferens\",\"Café\"]', '2018-03-01 08:15:44', '2018-03-01 08:15:44'),
(3, 'VAT Levels', '[25]', '2018-03-01 08:15:44', '2018-03-01 08:15:44'),
(4, 'Workplace Categories', '[1,2,3]', '2018-03-01 08:15:44', '2018-03-01 08:15:44'),
(5, 'Member Categories', '[\"A\",\"B\",\"C\"]', '2018-03-01 08:15:44', '2018-03-01 08:15:44'),
(6, 'Room Categories', '[1,2,3]', '2018-03-01 08:15:44', '2018-03-01 08:15:44'),
(7, 'Payment Conditions', '[\"30 dagar\",\"15 dagar\",\"10 dagar\",\"Omgående\"]', '2018-03-01 08:15:44', '2018-03-01 08:15:44'),
(8, 'Delivery Terms', '[\"\"]', '2018-03-01 08:15:44', '2018-03-01 08:15:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '-1',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_contact` tinyint(4) NOT NULL DEFAULT '0',
  `info` text COLLATE utf8mb4_unicode_ci,
  `category` tinyint(4) NOT NULL DEFAULT '0',
  `permission` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `company_id`, `phone`, `mobile`, `main_contact`, `info`, `category`, `permission`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'David Casper', 'jorge33@boyle.com', '$2y$10$ihx8RXsF2shW8b3ow8HVkODAeUQCqWSp9EBCbXmXUIxYfLE6XK1ym', 1, '+8595341545931', '+5505347058010', 1, 'Temporibus deserunt doloribus sed exercitationem id dolor. Culpa vero id deserunt nobis occaecati ut. Corrupti facilis sed ab quis in culpa velit. Laboriosam quidem tempora numquam inventore quis nemo.', 2, 0, NULL, NULL, '2018-03-01 08:15:45', '2018-03-01 08:15:45'),
(2, 'Hilton Wolff', 'lyla14@feeney.net', '$2y$10$/2xuOyOWrcV7VzvaGTr2a.Lyboue71OXBvPEd304M3zcvVPTb8vz.', 1, '+8883839429613', '+8849179895510', 0, 'Aut temporibus numquam eligendi doloribus. Modi fuga itaque quidem magni officia beatae quia sit. Necessitatibus optio et accusantium temporibus dolor. Atque ducimus adipisci aut qui et magnam sit.', 0, 0, NULL, NULL, '2018-03-01 08:15:45', '2018-03-01 08:15:45'),
(3, 'Elian Powlowski', 'elda22@hotmail.com', '$2y$10$ULT6cXeBBIseTZiMf3nwkeSsnKJegl8d9FwQKoJ5ryPEFinDcWE2G', 1, '+2465669837213', '+3733396734668', 0, 'Nam temporibus qui et minima sapiente est. Eaque est illo nobis aut rerum. Deserunt nostrum ea non saepe.', 0, 0, NULL, NULL, '2018-03-01 08:15:45', '2018-03-01 08:15:45'),
(4, 'Lizzie Towne', 'taryn91@sporer.info', '$2y$10$czYeZZzH0tsoZ48G69ysheZhM7cvKw8OpVDXYGc8nNCre7dpOoSDS', 1, '+6345498907612', '+8180989475280', 0, 'Dolores cum repellendus itaque quia. Eos voluptatem alias accusamus magnam. Repellendus doloremque sed autem qui earum quia ducimus consectetur.', 2, 0, NULL, NULL, '2018-03-01 08:15:45', '2018-03-01 08:15:45'),
(5, 'Tyra Altenwerth', 'nikolaus.edmond@pagac.info', '$2y$10$axkkIs13FUoWWEqj0y7P3OS6xe5Z.5B6ETkC4eUi2ljL6VQlMkUJ2', 1, '+1473306681788', '+4503248229714', 0, 'Quis aspernatur sit fuga. Eveniet doloremque rerum ut voluptate voluptatum iusto id maiores. Dolores nihil doloremque et corrupti perferendis id a quos. Eos aut ab rem hic.', 0, 0, NULL, NULL, '2018-03-01 08:15:45', '2018-03-01 08:15:45'),
(6, 'Mr. Cortez Mueller', 'morissette.eleanora@schaden.com', '$2y$10$FAluQ2FA1EuHSau1wspaPu7JZY8NBeOOOjZs.DXOLrFKfeAcXi2Tq', 1, '+6444327519984', '+6984364678796', 0, 'Vitae quis alias id alias atque ad ipsum. Ut pariatur earum ipsam repudiandae. Rerum porro eum autem dolorem ut veniam. Nulla quam accusamus praesentium exercitationem est.', 2, 0, NULL, NULL, '2018-03-01 08:15:46', '2018-03-01 08:15:46'),
(7, 'Prof. Khalil Bogan II', 'effertz.cristian@hotmail.com', '$2y$10$UH4nH7JKROAzrdDp/2SGVekOK4prc9mvqARSUCRtLVBlIRvFXe8d.', 2, '+6809438678701', '+1175228523340', 1, 'Voluptatibus atque accusamus eligendi qui eum qui eaque minus. Voluptatem ad sint ipsam. Necessitatibus et harum iste quia rerum. Fugit accusantium tempora assumenda dolores eligendi ut.', 1, 0, NULL, NULL, '2018-03-01 08:15:46', '2018-03-01 08:15:46'),
(8, 'Walter Olson', 'vadams@hotmail.com', '$2y$10$GCMchfQAUponmOA2oaqOf.1ARtQzfvvsUjIYIm9Q4wmx3SnAnhRea', 2, '+9167391495592', '+3178473526132', 0, 'Voluptas ipsum nihil quis commodi tenetur temporibus. Facilis qui quam voluptas rerum dolor saepe itaque.', 2, 0, NULL, NULL, '2018-03-01 08:15:46', '2018-03-01 08:15:46'),
(9, 'Dario Kiehn', 'boyle.kaley@cronin.com', '$2y$10$z6apDodNVD82Pe4AMvHUSuRHpD.5uspd9c4kIpDIRS0zmYuJGhbdO', 2, '+7076035094470', '+4446780368107', 0, 'Ipsum voluptates distinctio vitae. Sunt vero in ducimus dolorum soluta facilis. Autem quisquam ipsam animi quia officiis est. Quae autem nostrum temporibus.', 0, 0, NULL, NULL, '2018-03-01 08:15:46', '2018-03-01 08:15:46'),
(10, 'Kitty Donnelly', 'pinkie.crona@murphy.com', '$2y$10$vOwY1YXEs8x9NDixlz9LFOtnjAQlBJrd8kgZsOHUt4eEgk8jueu76', 2, '+5653081075007', '+8797428188587', 0, 'Ut perspiciatis aliquam alias similique eius exercitationem consequatur. Voluptatem adipisci totam velit dolorum perspiciatis.', 1, 0, NULL, NULL, '2018-03-01 08:15:46', '2018-03-01 08:15:46'),
(11, 'Mrs. Alessandra Paucek', 'skiles.gregoria@yahoo.com', '$2y$10$JI8KgRQBT9SerSU2JgJOTemYy8P4YvZ04IAtEORTwBV0u6HRZXOIq', 2, '+6951656326268', '+7199977065339', 0, 'Fuga quis cumque molestiae aliquid iste sit. Sint voluptatum ea voluptatem maxime expedita qui inventore. Molestiae unde voluptatibus iusto rerum repudiandae quo accusamus.', 1, 0, NULL, NULL, '2018-03-01 08:15:47', '2018-03-01 08:15:47'),
(12, 'Georgiana Trantow', 'isadore25@kunze.info', '$2y$10$ekzM0sfWBI091cPFUeEzm.wyK3ddLdx/f1S.0LoMQv400VizbiKnu', 2, '+2943641058906', '+5505138796789', 0, 'Velit autem temporibus ut. Et et rerum natus iusto. Sapiente ipsum quidem libero et.', 0, 0, NULL, NULL, '2018-03-01 08:15:47', '2018-03-01 08:15:47'),
(13, 'Malcolm Zulauf', 'cole.macie@strosin.com', '$2y$10$cvZl9cRm/ozvxMFbmP98cOsMod/Bq5XqkmYutItPSbgeePR/bHDuq', 2, '+7995353552075', '+1677614960190', 1, 'Quia eos fuga deserunt nesciunt quidem aliquam eum. Reiciendis quia voluptas voluptas. Rem odio placeat vel ut unde. Molestias ipsam culpa sed id voluptatum.', 1, 0, NULL, NULL, '2018-03-01 08:15:47', '2018-03-01 08:15:47'),
(14, 'Foster Douglas', 'kling.anibal@crooks.com', '$2y$10$acJ9k2rjgnM08UiWIzvCX.11ZM80C4LAfeKLsXI5En3aypUBEo3PS', 3, '+9963402298397', '+5955422526606', 0, 'Quae beatae eveniet cum quaerat sint. Deserunt natus repellat non. Error soluta hic fugiat beatae at quasi perspiciatis et. Enim ad ut molestias eos.', 1, 0, NULL, NULL, '2018-03-01 08:15:47', '2018-03-01 08:15:47'),
(15, 'Annabelle Kunze', 'talon.thiel@kirlin.org', '$2y$10$GnYOtAW3HGgTXQF18b72YOvEM7gKvh9ASkfQsFjxX0egdgXgAQCjK', 3, '+8419827992525', '+6457338124967', 0, 'Recusandae quibusdam adipisci at et. Deleniti dolore omnis recusandae dicta molestiae exercitationem. Nam ut eos ut voluptatem corrupti quae.', 0, 0, NULL, NULL, '2018-03-01 08:15:47', '2018-03-01 08:15:47'),
(16, 'Sadye Denesik', 'jazmyne48@yahoo.com', '$2y$10$bV1Aq5rFLpC9F8LjFKRvque8IKv.YB29o/9W0jiZDedYy89yXBXdG', 3, '+7860286007439', '+2791273081623', 0, 'Eum et beatae itaque eos non tempora. Sunt sed ducimus temporibus non.', 2, 0, NULL, NULL, '2018-03-01 08:15:47', '2018-03-01 08:15:47'),
(17, 'Mr. Ford Cronin', 'ilittel@buckridge.net', '$2y$10$iM5JyL7N6oWCdy9hdFh9OeY/ZNarXx0Ej181QWYortuL5bXktdM9u', 3, '+4803486961083', '+9592336065872', 0, 'Quis odio ut ullam non natus cupiditate. Vel velit sequi quibusdam commodi odio. Autem ut quod odit aut magni. Ratione qui saepe consequatur est praesentium.', 1, 0, NULL, NULL, '2018-03-01 08:15:48', '2018-03-01 08:15:48'),
(18, 'Mrs. Kailey Hills II', 'wlang@gmail.com', '$2y$10$hZNtasfHNkhmOkKvxi6Mm.jL2ag0lWOGzdpQFohyw9VCvPjnZ9jw.', 3, '+5920142270655', '+6889094805536', 0, 'Omnis neque et quia veniam sequi labore magnam vel. Perferendis quia id et incidunt. Amet reprehenderit dolorum ipsa temporibus. Ad atque inventore occaecati et.', 2, 0, NULL, NULL, '2018-03-01 08:15:48', '2018-03-01 08:15:48'),
(19, 'Kiara Gleichner', 'cayla65@hotmail.com', '$2y$10$98P5SHHc1PY6C0cYnbi0s.BbWKhHqyAQKw/yTa09na0nDmTSlTZ0K', 3, '+8913585129975', '+1324445046609', 1, 'Expedita eligendi molestiae sed illum eos eos ducimus. Quae officia aut voluptates sit expedita et vero. Maxime velit ut et atque. Placeat laborum quia ipsa quia qui.', 2, 0, NULL, NULL, '2018-03-01 08:15:48', '2018-03-01 08:15:48'),
(20, 'Houston Gleason', 'gturner@gmail.com', '$2y$10$6cnHb/Lo7ufydUqX9bJQYeaO3vv5gOQxy0hw6wiRFahXGFJYHYkRi', 3, '+9227835499250', '+8591793703023', 0, 'Aut natus vel impedit tenetur. Deserunt non aliquid id. Sed est ut aut sed ipsum. Voluptatibus qui non et sit aut. Dolores sunt facilis error dolores dolor veniam amet.', 1, 0, NULL, NULL, '2018-03-01 08:15:48', '2018-03-01 08:15:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_groups`
--
ALTER TABLE `article_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `economies`
--
ALTER TABLE `economies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `economy_articles`
--
ALTER TABLE `economy_articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mackerias`
--
ALTER TABLE `mackerias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mackeria_articles`
--
ALTER TABLE `mackeria_articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `package_articles`
--
ALTER TABLE `package_articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `article_groups`
--
ALTER TABLE `article_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `economies`
--
ALTER TABLE `economies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `economy_articles`
--
ALTER TABLE `economy_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `mackerias`
--
ALTER TABLE `mackerias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `mackeria_articles`
--
ALTER TABLE `mackeria_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `package_articles`
--
ALTER TABLE `package_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
