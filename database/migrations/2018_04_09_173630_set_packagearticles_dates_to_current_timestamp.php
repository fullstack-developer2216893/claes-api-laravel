<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetPackagearticlesDatesToCurrentTimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_articles', function (Blueprint $table) {
            $table->datetime('created_at')->default('CURRENT_TIMESTAMP')->change();
            $table->datetime('updated_at')->default('CURRENT_TIMESTAMP')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_articles', function (Blueprint $table) {
            //
        });
    }
}
