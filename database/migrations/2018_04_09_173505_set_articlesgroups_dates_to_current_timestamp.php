<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetArticlesgroupsDatesToCurrentTimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_groups', function (Blueprint $table) {
            $table->datetime('created_at')->default('CURRENT_TIMESTAMP')->change();
            $table->datetime('updated_at')->default('CURRENT_TIMESTAMP')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_groups', function (Blueprint $table) {
            //
        });
    }
}
