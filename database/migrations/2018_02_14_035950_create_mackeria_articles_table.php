<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMackeriaArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mackeria_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mackeria_id')->default(0);
            $table->integer('article_id')->default(0);
            $table->string('date', 10)->nullable();
            $table->integer('quantity')->default(0);
            $table->string('name', 100)->nullable();
            $table->integer('price')->default(0);
            $table->integer('total')->default(0);
            $table->integer('vat_total')->default(0);
            $table->integer('total_incl_vat')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mackeria_articles');
    }
}
