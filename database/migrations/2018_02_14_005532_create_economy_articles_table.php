<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEconomyArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('economy_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->default(0);
            $table->integer('quote_id')->default(0);
            $table->integer('booking_id')->default(0);
            $table->integer('article_group')->default(0);
            $table->string('name', 100)->nullable();
            $table->string('package', 100)->nullable();
            $table->integer('quantity')->default(0);
            $table->integer('article_id')->default(0);
            $table->timestamp('date_fr')->nullable();
            $table->timestamp('date_to')->nullable();
            $table->string('serv_time', 5)->nullable();
            $table->integer('discount')->default(0);
            $table->integer('unit')->default(0);
            $table->integer('price')->default(0);
            $table->integer('vat_level')->default(0);
            $table->integer('total')->default(0);
            $table->integer('vat_total')->default(0);
            $table->integer('total_incl_vat')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('economy_articles');
    }
}
