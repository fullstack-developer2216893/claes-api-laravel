<?php

use Illuminate\Database\Seeder;
use App\MackeriaArticle as MackeriaArticle;

class MackeriaArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        MackeriaArticle::truncate();
        
        $faker = \Faker\Factory::create();

        $numMackerias = 50;

        // And now, let's create the items in our database:
        for ($i = 0; $i < $numMackerias; $i++) {

            $price = $faker->randomNumber(4);
            $quantity = $faker->randomNumber(1);

            $total = $price * $quantity;
            $vat_total = $total * 0.25;
            $total_incl_vat = $total + $vat_total;

            MackeriaArticle::create([
                'mackeria_id' => $faker->randomNumber(1),
                'article_id' => $faker->randomNumber(1),
                'date' => $faker->date(),
                'quantity' => $quantity,
                'name' => ucwords($faker->sentence()),
                'price' => $price,
                'total' => $total,
                'vat_total' => $vat_total,
                'total_incl_vat' => $total_incl_vat
            ]);
        }
    }
}
