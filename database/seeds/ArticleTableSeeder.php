<?php

use Illuminate\Database\Seeder;
use App\Article as Article;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Article::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 10; $i++) {
            Article::create([
                'date' => $faker->date(),
                'name' => $faker->sentence(3),
                'unit' => $faker->numberBetween(0, 4),
                'price' => $faker->numberBetween(1, 1000),
                'vat_level' => 0,
                'article_group' => $faker->numberBetween(0, 12),
                'result_unit' => $faker->numberBetween(0, 3),
                'info' => $faker->paragraph(),
                'layout1' => $faker->numberBetween(0, 99),
                'layout2' => $faker->numberBetween(0, 99),
                'layout3' => $faker->numberBetween(0, 99),
                'bookable' => $faker->numberBetween(0, 1),
                'image' => $faker->image(),
            ]);
        }
    }
}
