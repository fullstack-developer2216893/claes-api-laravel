<?php

use Illuminate\Database\Seeder;
use App\ArticleGroup as ArticleGroup;

class ArticleGroupTableSeeder extends Seeder
{
    public $articleGroups = [

        // Top-level
        ['id' => 1, 'name' => 'Arbetsplatser'],
        ['id' => 2, 'name' => 'Konferensrum'],
        ['id' => 3, 'name' => 'Konferenspaket'],
        ['id' => 4, 'name' => 'AV-utrustning'],
        ['id' => 5, 'name' => 'Café & Mackeria'],

        // Arbetsplatser's children
        ['id' => 6, 'name' => 'Kontorsrum', 'parent_id' => 1],
        ['id' => 7, 'name' => 'Arbetsplatser', 'parent_id' => 1],
        ['id' => 8, 'name' => 'Flexiplatser', 'parent_id' => 1]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        ArticleGroup::truncate();
        
        // And now, let's create the items in our database:
        foreach ($this->articleGroups as $group) {

            $id = $group['id'];
            $name = $group['name'];
            $parent_id = (isset($group['parent_id'])) ? $group['parent_id'] : -1; 

            ArticleGroup::create([
                'id' => $id,
                'name' => $name,
                'parent_id' => $parent_id
            ]);
        }
    }
}
