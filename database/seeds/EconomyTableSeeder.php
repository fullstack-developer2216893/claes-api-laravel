<?php

use Illuminate\Database\Seeder;
use App\Economy as Economy;

class EconomyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Economy::truncate();
        
        $faker = \Faker\Factory::create();

        $numCustomers = 3;

        $numEconomies = 10;

        // And now, let's create the items in our database:
        for ($i = 0; $i < $numEconomies; $i++) {

            $total = $faker->randomNumber(4);
            $vat_total = $total * 0.25;
            $total_incl_vat = $total + $vat_total;

            Economy::create([
                'date' => $faker->date(),
                'serial_quote' => $faker->randomNumber(5),
                'serial_version_quote' => $faker->randomNumber(5),
                'serial_booking' => $faker->randomNumber(5),
                'serial_order' => $faker->randomNumber(5),
                'customer_id' => ceil((($i+1) / $numEconomies) * $numCustomers),
                'name' => ucwords($faker->sentence()),
                'payment_conditions' => $faker->paragraph(),
                'valid_to' => $faker->date(),
                'category' => $faker->randomNumber(1),
                'type' => $faker->randomNumber(1),
                'status' => $faker->randomNumber(1),
                'delivery_terms' => $faker->paragraph(),
                'room_info' => $faker->paragraph(),
                'av_info' => $faker->paragraph(),
                'price_info' => $faker->paragraph(),
                'meal_info' => $faker->paragraph(),
                'number_of_persons' => $faker->randomNumber(2),
                'number_of_persons_text' => ucwords($faker->sentence()),
                'info' => $faker->paragraph(),
                'total' => $total,
                'vat_total' => $vat_total,
                'total_incl_vat' => $total_incl_vat
            ]);
        }
    }
}