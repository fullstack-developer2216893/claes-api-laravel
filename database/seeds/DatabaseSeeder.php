<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArticleGroupTableSeeder::class);
        $this->call(ArticleTableSeeder::class);
        $this->call(BookingTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(EconomyArticleTableSeeder::class);
        $this->call(EconomyTableSeeder::class);
        $this->call(EventTableSeeder::class);
        $this->call(MackeriaArticleTableSeeder::class);
        $this->call(MackeriaTableSeeder::class);
        $this->call(PackageArticleTableSeeder::class);
        $this->call(ParticipantTableSeeder::class);
        $this->call(PartnerTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
