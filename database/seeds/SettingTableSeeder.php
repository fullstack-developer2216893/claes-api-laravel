<?php

use Illuminate\Database\Seeder;
use App\Setting as Setting;

class SettingTableSeeder extends Seeder
{
    public $settings = [
        'Units' => '["Styck","Timme","Dag","Månad"]',
        'Result Units' => '["Kontorshotell","Konferens","Café"]',
        'VAT Levels' => '[25]',
        'Workplace Categories' => '[1,2,3]',
        'Member Categories' => '["A","B","C"]',
        'Room Categories' => '[1,2,3]',
        'Payment Conditions' => '["30 dagar","15 dagar","10 dagar","Omgående"]',
        'Delivery Terms' => '[""]'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Setting::truncate();
        
        // And now, let's create the items in our database:
        foreach ($this->settings as $name => $value) {

            Setting::create([
                'name' => $name,
                'value' => $value
            ]);
        }
    }
}
