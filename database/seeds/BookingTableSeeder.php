<?php

use Illuminate\Database\Seeder;
use App\Booking as Booking;

class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Booking::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 10; $i++) {

            Booking::create([
                'article_id' => $faker->randomNumber(1),
                'economy_id' => $faker->randomNumber(1),
                'date_fr' => $faker->dateTime(),
                'date_to' => $faker->dateTime()
            ]);
        }
    }
}