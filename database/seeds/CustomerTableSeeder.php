<?php

use Illuminate\Database\Seeder;
use App\Customer as Customer;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Customer::truncate();
        
        $faker = \Faker\Factory::create();

        // And now, let's create the items in our database:
        for ($i = 0; $i < 3; $i++) {
            Customer::create([
                'date' => $faker->date(),
                'workplace' => ucwords($faker->word()),
                'room_category' => $faker->randomElement([0, 1, 2]),
                'member_category' => $faker->randomElement([0, 1, 2]),
                'name' => $faker->company(),
                'registration_number' => $faker->randomNumber(9),
                'phone' => $faker->e164PhoneNumber(),
                'email' => 'info@'.$faker->word().'.com',
                'address' => $faker->streetAddress(),
                'zip_code' => $faker->randomNumber(5),
                'town' => $faker->city(),
                'visiting_address' => $faker->streetAddress(),
                'visiting_zip_code' => $faker->randomNumber(5),
                'visiting_town' => $faker->city(),
                'info' => $faker->paragraph()
            ]);
        }
    }
}
