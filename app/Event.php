<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 
        'name', 
        'location', 
        'time_from', 
        'time_to',
        'max_persons', 
        'price', 
        'info', 
        'image'
    ];

    public function participant(){
        return $this->hasMany('App\Participant','event_id');
    }
}