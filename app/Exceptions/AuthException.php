<?php

namespace App\Exceptions;

//use Illuminate\Auth\AuthenticationException as AuthenticationException;
use Exception;

class AuthException extends Exception
{
    const EXCEPTION_AUTH_JSON = 10001;

    /**
     * Create a new authentication exception.
     *
     * @param  string  $message
     * @param  int  $code
     * @param  array  $guards
     * @return void
   
    public function __construct($message = 'Unauthenticated.', int $code = -1, array $guards = [])
    {
        parent::__construct($message, $guards);

        $this->code = $code;
    }
      */
}
