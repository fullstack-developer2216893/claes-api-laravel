<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EconomyArticle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 
        'quote_id', 
        'booking_id', 
        'article_group', 
        'name', 
        'package',
        'quantity', 
        'article_id', 
        'date_fr', 
        'date_to', 
        'serv_time', 
        'discount', 
        'unit', 
        'price', 
        'vat_level', 
        'total', 
        'vat_total', 
        'total_incl_vat'
    ];
}