<?php

namespace App\Http\Controllers;

use App\Economy as Economy;
use Illuminate\Http\Request;

class EconomyController extends Controller
{
    protected static $model = "App\Economy";

    protected static $searchFields = ['customer_id'];

    protected static $joins = [
        ['leftJoin', ['customers', 'customers.id', '=', 'economies.customer_id']]
    ];

    protected static $select = [
        'economies.*',
        'customers.name as customer_name',
        'customers.email as customer_email',
        'customers.phone as customer_phone'
    ];
}
