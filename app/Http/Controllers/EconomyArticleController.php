<?php

namespace App\Http\Controllers;

use App\EconomyArticle as EconomyArticle;
use Illuminate\Http\Request;

class EconomyArticleController extends Controller
{
    protected static $model = "App\EconomyArticle";
    
    protected static $searchFields = ['quote_id', 'article_id'];

    protected static $joins = [
        ['leftJoin', ['articles', 'articles.id', '=', 'economy_articles.article_id']],
        ['leftJoin', ['economies', 'economies.id', '=', 'economy_articles.quote_id']]
    ];

    protected static $select = [
        'economy_articles.*',
        'articles.name as article_name',
        'articles.image as article_image ',
        'economies.name as economy_name'
    ];
}
