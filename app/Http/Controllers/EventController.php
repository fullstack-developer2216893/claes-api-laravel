<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event as Event;
use App\Participant as participant;

class EventController extends Controller
{
    protected static $model = "App\Event";

    public function index(Request $request){
    	$events=Event::with(['participant'])->get();
    	$eventArr=array();
    	if($events){
    		foreach ($events as $event) {
    			$event['participants']=$this->countParticipants($event['id']);
    			$eventArr[]=$event;
    		}
    	}
    	return $eventArr;
    }

    public function show(int $id){
    	return Event::with(['participant'])->find($id);
    }

    private function countParticipants($eventId){
    	return $participants=participant::where('event_id',$eventId)->count();
    }

    public function deleteParticipant($evId,$participantId){
    	$deletedRow = participant::where(array('event_id'=>$evId,'id'=>$participantId))->delete();
    	if($deletedRow){
    		return 'true';
    	}
    }

}
