<?php

namespace App\Http\Controllers;

use App\User as User;
use App\Exceptions\Handler as Handler;
use App\Exceptions\AuthException as AuthException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator; 
use Illuminate\Support\Facades\Auth as Auth;
use Exception;

class UserController extends Controller
{
    protected static $model = 'App\User';
    
    protected static $searchFields = ['customer_id'];

    /**
     * Login User
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(!Auth::attempt(['email' => request('email'), 'password' => request('password')]))
        {
            throw new AuthException('Unauthorized');
        }

        $user = Auth::user();
        $success['token'] = $user->createToken('FrodoAPI')->accessToken;
        return response()->json(['success' => $success], 200);
    }

    /**
     * Register User
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails())
        {
            throw new AuthException($validator->errors(), AuthException::EXCEPTION_AUTH_JSON);          
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('FrodoAPI')->accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], 200);
    }

}