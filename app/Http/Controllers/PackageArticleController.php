<?php

namespace App\Http\Controllers;

use App\PackageArticle as PackageArticle;
use Illuminate\Http\Request;

class PackageArticleController extends Controller
{
    protected static $model = "App\PackageArticle";

    protected static $joins = [
        ['leftjoin', ['articles', 'articles.id', '=', 'package_articles.package_id']]
    ];

    protected static $select = [
        'articles.*',
        'package_articles.id as package_id'
    ];

    public function getPackages($articleId){
    	return PackageArticle::leftjoin('articles', 'articles.id', '=', 'package_articles.package_id')
    	->select('articles.*','package_articles.id as package_id')
		->where('article_id',$articleId)->get();
    }


}
