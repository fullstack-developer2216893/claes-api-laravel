<?php

namespace App\Http\Controllers;

use App\MackeriaArticle as MackeriaArticle;
use App\Article as Article;
use Illuminate\Http\Request;
use DB;

class MackeriaArticleController extends Controller
{
    protected static $model = "App\MackeriaArticle";

    public function store(Request $request){
        $allPostedValues=$request->all();
        $MackeriaArticleArr=array();
        if($allPostedValues['article_id']){
            $article=Article::find($allPostedValues['article_id']);
        	$MackeriaArticleArr['article_id']=$article->id;
        	$MackeriaArticleArr['mackeria_id']=$allPostedValues['mackeria_id'];
        	$MackeriaArticleArr['quantity']=$allPostedValues['quantity'];
        	$MackeriaArticleArr['name']=$article->name;
        	$MackeriaArticleArr['date']=$article->date;
        	$MackeriaArticleArr['price']=$article->price;
        	$MackeriaArticleArr['total']=$article->price*$allPostedValues['quantity'];
        	$MackeriaArticleArr['vat_total']=0.25*$MackeriaArticleArr['total'];
        	$MackeriaArticleArr['total_incl_vat']=$MackeriaArticleArr['total']+$MackeriaArticleArr['vat_total'];
            if($allPostedValues['id']){
                DB::table('mackeria_articles')
                ->where('id', $allPostedValues['id'])
                ->update($MackeriaArticleArr);
                $id=$allPostedValues['id'];
            }else{
                $id=DB::table('mackeria_articles')->insertGetId($MackeriaArticleArr);
            }
        	
        	return MackeriaArticle::find($id);
        }
    }

    public function articleMackeria($mid){
    	return MackeriaArticle::where('mackeria_id',$mid)->get();
    }
}
