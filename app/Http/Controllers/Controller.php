<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Test as Test;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected static $model = '';
    protected static $searchFields = [];
    protected static $joins = [];
    protected static $select = [];
    
    public function index(Request $request)
    {        
        $filters = $this->getFilters($request, static::$searchFields);

        $query = static::$model::where($filters);

        if(!empty(static::$joins))
        {
            foreach(static::$joins as $join)
            {
                $type = $join[0];
                $data = $join[1];
                call_user_func_array([$query, $type], $data);
            }
        }

        if(!empty(static::$select))
        {
            call_user_func_array([$query, 'select'], static::$select);
        }

        return $query->get();
    }

    public function show(int $id)
    {
        $obj = static::$model::where(['id' => $id])->first();
        return response()->json($obj, 200);
    }

    public function store(Request $request)
    {
        $obj = static::$model::create($request->all());
        return response()->json($obj, 201);
    }

    public function update(Request $request, int $id)
    {
        static::$model::where(['id' => $id])->update($request->all());
        return $this->show($id);
    }

    public function destroy(Request $request, int $id)
    {
        static::$model::where(['id' => $id])->delete();
        return response()->json(null, 204);
    }

    protected function getFilters($request, $filters)
    {
        $conditions = [];

        foreach($filters as $filter)
        {
            $column = '';
            $operation = '';
            $decorator = '';

            if(is_array($filter))
            {
                $column = $filter[0];
                $operation = $filter[1];
                $decorator = $filter[2];
            }
            else
            {
                $column = $filter;
                $operation = '=';
                $decorator = '';
            }

            if ($request->has($column))
            {
                $conditions[] = [$column, $operation, $decorator.$request->input($column).$decorator];
            }
        }

        return $conditions;
    }
}
