<?php

namespace App\Http\Controllers;

use App\Setting as Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    protected static $model = "App\Setting";

    /*
    @method:filterByName
    @description:-get settings table info with name
    @params:-$name
    */
    public function filterByName($name){
    	$name=ucwords(str_replace('-', ' ',$name));
    	return Setting::where('name',$name)->first();
    }
}
