<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer as Customer;

class CustomerController extends Controller
{
    protected static $model = "App\Customer";
}
