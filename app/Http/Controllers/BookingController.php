<?php

namespace App\Http\Controllers;

use App\Booking as Booking;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    protected static $model = "App\Booking";

    protected static $searchFields = ['economy_id', 'article_id'];

    protected static $joins = [
        ['leftJoin', ['articles', 'articles.id', '=', 'bookings.article_id']],
        ['leftJoin', ['economies', 'economies.id', '=', 'bookings.economy_id']]
    ];

    protected static $select = [
        'bookings.*',
        'articles.name as article_name',
        'articles.image as article_image ',
        'economies.name as economy_name'
    ];
}