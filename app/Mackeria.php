<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mackeria extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 
        'customer_id',
        'contact_id', 
        'delivery_date', 
        'delivered', 
        'info', 
        'total', 
        'vat_total', 
        'total_incl_vat'
    ];

    public function mackeriaArticles(){
        return $this->hasMany('App\MackeriaArticle','mackeria_id');
    }
}
